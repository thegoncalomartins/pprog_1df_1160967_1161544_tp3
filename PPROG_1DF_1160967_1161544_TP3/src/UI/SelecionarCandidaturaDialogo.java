/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.Candidatura;
import Model.CentroDeEventos;
import Model.Evento;
import com.sun.glass.events.KeyEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * A janela de diálogo que permite ao utilizador (FAE) selecionar a candidatura a decidir.
 * 
 * @author m
 */
public class SelecionarCandidaturaDialogo extends JDialog {

    /**
     * O centro de eventos.
     */
    private CentroDeEventos ce;

    /**
     * O evento do qual se vai decidir uma candidatura.
     */
    private Evento ev;

    /**
     * A JList com a lista de candidaturas a mostrar ao utilizador.
     */
    private JList listaCandidaturas;

    /**
     * O botão que abre a janela seguinte.
     */
    private JButton btnSeguinte;

    /**
     * A largura da janela (em pixeis).
     */
    private static final int LARGURA_JANELA = 900;

    /**
     * A altura da janela (em pixeis).
     */
    private static final int ALTURA_JANELA = 550;

    /**
     * Construtor que recebe por parâmetro o título da janela e o centro de eventos.
     *
     * @param framePai a janela que chama este diálogo
     * @param titulo o título da janela
     * @param ce o centro de eventos
     * @param ev o evento
     */
    public SelecionarCandidaturaDialogo(DecidirCandidaturaDialogo framePai, String titulo, CentroDeEventos ce, Evento ev) {
        super(framePai);

        this.ce = ce;

        this.ev = ev;

        criarComponentes(titulo);

        iniciarDefinicoes();

    }

    /**
     * Cria os componentes da janela.
     *
     * @param titulo o título da janela
     */
    private void criarComponentes(String titulo) {
        add(criarPainelNorte(titulo), BorderLayout.NORTH);
        add(criarPainelSul(), BorderLayout.SOUTH);
        add(criarPainelCentro(), BorderLayout.CENTER);

    }

    /**
     * Definições da janela.
     */
    private void iniciarDefinicoes() {
        ImageIcon img = new ImageIcon("calendar.png");
        setIconImage(img.getImage());
        setSize(LARGURA_JANELA, ALTURA_JANELA);
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setModal(true);
        setVisible(true);
    }

    /**
     * Devolve o painel que é adicionado à região norte da JFrame.
     *
     * @param titulo o título da janela
     * @return o painel criado
     */
    private JPanel criarPainelNorte(String titulo) {
        JPanel painelNorte = new JPanel();
        painelNorte.setBackground(new Color(180, 202, 237));
        painelNorte.add(criarLabelNorte(titulo));

        return painelNorte;
    }

    /**
     * Devolve a label com o título da janela.
     *
     * @param titulo o título da janela
     * @return a labelc criada
     */
    private JLabel criarLabelNorte(String titulo) {
        JLabel lblNorte = new JLabel(titulo);

        lblNorte.setFont(new Font("Segoe UI", Font.ITALIC, 24));

        return lblNorte;
    }

    /**
     * Devolve o painel que é adicionado à região centro da JFrame.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCentro() {
        JPanel painelCentro = new JPanel();
        painelCentro.setLayout(new BoxLayout(painelCentro, BoxLayout.Y_AXIS));

        painelCentro.add(criarPainelCentroSuperior());
        painelCentro.add(criarPainelListaCandidaturas());

        return painelCentro;
    }

    /**
     * Devolve o painel que é adicionado ao topo do painel central da JFrame.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCentroSuperior() {
        JPanel painelCentroSuperior = new JPanel();
        JLabel lblTitulo = new JLabel("Selecione a candidatura que deseja decidir");
        lblTitulo.setFont(new Font("Segoe UI", Font.PLAIN, 19));

        painelCentroSuperior.add(lblTitulo);

        return painelCentroSuperior;
    }

    /**
     * Devolve o painel onde é mostrada a lista de candidaturas.
     *
     * @return o painel criado
     */
    private JPanel criarPainelListaCandidaturas() {
        JPanel painelListaCandidaturas = new JPanel();

        painelListaCandidaturas.add(criarListaCandidaturas());

        return painelListaCandidaturas;
    }

    /**
     * Devolve o JScrollPane onde se mostra a lista de candidaturas ao utilizador.
     * Código cellRenderer retirado de https://stackoverflow.com/questions/9560474/java-jlist-unwanted-tostring-conversion, autor Jeff Storey
     * @return o JScrollPane criado
     */
    private JScrollPane criarListaCandidaturas() {

        ModeloListaCandidaturas modeloListaCandidaturas = new ModeloListaCandidaturas(ev.getListaCandidaturas());
        listaCandidaturas = new JList(modeloListaCandidaturas);

        class Renderer extends DefaultListCellRenderer {

            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                setText(String.format("%s", ((Candidatura)value).getEmpresa()));
                return c;
            }

        }

        listaCandidaturas.setCellRenderer(new Renderer());

        listaCandidaturas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JScrollPane scrPane = new JScrollPane(listaCandidaturas);

        scrPane.setPreferredSize(new Dimension(400, 350));
       

        

        return scrPane;
    }

    /**
     * Devolve o painel que é adicionado à região sul da JFrame.
     *
     * @return o painel criado
     */
    private JPanel criarPainelSul() {

        JPanel painelSul = new JPanel();
        FlowLayout fl = (FlowLayout) painelSul.getLayout();
        fl.setHgap(27);
        painelSul.setBackground(new Color(18, 69, 150));

        painelSul.add(criarBotaoRetroceder());
        painelSul.add(criarBotaoSeguinte());

        return painelSul;
    }

    /**
     * Cria e devolve o botão "Retroceder", que permite ao utilizador voltar para a página anterior. ícone do botão: Autor - Cursor Creative http://www.flaticon.com/authors/cursor-creative Disponível em: http://www.flaticon.com/free-icon/return_339736#term=back&page=4&position=83
     *
     * @return o botão criado
     */
    private JButton criarBotaoRetroceder() {
        JButton btnBack = new JButton("Retroceder");

        btnBack.setText("Retroceder");
        btnBack.setToolTipText("Volta para a página anterior.");
        btnBack.setMnemonic(KeyEvent.VK_R);
        btnBack.setIcon(new ImageIcon("return.png"));

        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SelecionarCandidaturaDialogo.this.dispose();
            }
        });

        return btnBack;
    }

    /**
     * Cria e devolve o botão seguinte que abre a janela seguinte. 
     * ícone do botão: Autor - Roundicons http://www.flaticon.com/authors/roundicons 
     * Disponível em: http://www.flaticon.com/free-icon/next_189674#term=next&page=1&position=85
     * @return o botão criado
     */
    private JButton criarBotaoSeguinte() {
        btnSeguinte = new JButton("Seguinte");
        btnSeguinte.setToolTipText("Avança para a página seguinte.");
        btnSeguinte.setMnemonic(KeyEvent.VK_S);

        btnSeguinte.setIcon(new ImageIcon("next.png"));

        btnSeguinte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                if (listaCandidaturas.getSelectedValue() == null) {
                    JOptionPane.showMessageDialog(SelecionarCandidaturaDialogo.this, "Tem de selecionar uma candidatura para prosseguir!", "ATENÇÃO!", JOptionPane.WARNING_MESSAGE, new ImageIcon("warning.png"));
                } else {
                    new InserirDecisaoDialogo(SelecionarCandidaturaDialogo.this, "Decisão de Candidatura", ce, ev, (Candidatura) listaCandidaturas.getSelectedValue());

                }

            }
        });
        return btnSeguinte;
    }

}
