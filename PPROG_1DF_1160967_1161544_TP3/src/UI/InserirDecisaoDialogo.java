/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.Candidatura;
import Model.CentroDeEventos;
import Model.Decisao;
import Model.Evento;
import com.sun.glass.events.KeyEvent;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;

/**
 * A janela de diálogo que permite ao utilizador (FAE) introduzir a decisão de uma candidatura.
 * @author m
 */
public class InserirDecisaoDialogo extends JDialog {

    /**
     * O centro de eventos.
     */
    private CentroDeEventos ce;

    /**
     * O evento do qual se vai decidir uma candidatura.
     */
    private Evento ev;

    /**
     * A candidatura a ser decidida.
     */
    private Candidatura cand;

    /**
     * O RadioButton que corresponde ao deferimento da candidatura.
     */
    private JRadioButton btnSim;

    /**
     * O RadioButton que corresponde ao indeferimento da candidatura.
     */
    private JRadioButton btnNao;

    /**
     * O campo de texto onde vai ser escrita a justificação da decisão da candidatura.
     */
    private JTextArea txtTextoJ;

    /**
     * A largura da janela (em pixeis).
     */
    private static final int LARGURA_JANELA = 900;

    /**
     * A altura da janela (em pixeis).
     */
    private static final int ALTURA_JANELA = 550;

    /**
     * Construtor que recebe por parâmetro o título da janela e o centro de eventos.
     *
     * @param framePai
     * @param titulo o título da janela
     * @param ce o centro de eventos
     * @param ev o evento que possui a candidatura
     * @param cand a candidatura selecionada na janela de diálogo anterior
     */
    public InserirDecisaoDialogo(SelecionarCandidaturaDialogo framePai, String titulo, CentroDeEventos ce, Evento ev, Candidatura cand) {
        super(framePai);

        this.ce = ce;

        this.ev = ev;

        this.cand = cand;

        criarComponentes(titulo);

        iniciarDefinicoes();

    }

    /**
     * Cria os componentes da janela.
     *
     * @param titulo o título da janela
     */
    private void criarComponentes(String titulo) {
        add(criarPainelNorte(titulo), BorderLayout.NORTH);
        add(criarPainelCentro(), BorderLayout.CENTER);
        add(criarPainelSul(), BorderLayout.SOUTH);

    }

    /**
     * Mostra uma caixa de diálogo ao utilizador com as informações acerca da candidatura
     *
     * @param infoCandidatura as informações da candidatura
     * @param titulo o título da Caixa de Diálogo
     * @param icone o ícone da caixa de diálogo
     */
    private void mostrarCaixaDialogoInformacoesCandidatura(String infoCandidatura, String titulo, ImageIcon icone) {
        JDialog infoCandidaturaDialogo = new JDialog(this);

        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel(titulo));

        JPanel painelCandidatura = new JPanel();

        JTextArea txtInfoCandidatura = new JTextArea(infoCandidatura);
        txtInfoCandidatura.setEditable(false);

        // cria uma linha de contorno à volta da caixa de texto
        Border border = BorderFactory.createLineBorder(Color.GRAY);
        txtInfoCandidatura.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        painelCandidatura.add(txtInfoCandidatura);

        infoCandidaturaDialogo.add(painelTitulo, BorderLayout.NORTH);
        infoCandidaturaDialogo.add(painelCandidatura, BorderLayout.CENTER);

        JPanel painelBotaoOK = new JPanel();

        JButton btnOK = new JButton("OK");
        btnOK.setMnemonic(KeyEvent.VK_O);

        btnOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                infoCandidaturaDialogo.dispose();
            }

        });

        painelBotaoOK.add(btnOK);

        infoCandidaturaDialogo.add(painelBotaoOK, BorderLayout.SOUTH);

        infoCandidaturaDialogo.setIconImage(icone.getImage());
        infoCandidaturaDialogo.pack();
        infoCandidaturaDialogo.setMinimumSize(new Dimension(infoCandidaturaDialogo.getWidth(), infoCandidaturaDialogo.getHeight()));
        infoCandidaturaDialogo.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        infoCandidaturaDialogo.setLocationRelativeTo(this);
        infoCandidaturaDialogo.setVisible(true);
    }

    /**
     * Definições da janela.
     */
    private void iniciarDefinicoes() {
        setIconImage(new ImageIcon("calendar.png").getImage());
        setSize(LARGURA_JANELA, ALTURA_JANELA);
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocationRelativeTo(null);

        addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
                mostrarCaixaDialogoInformacoesCandidatura(cand.toString(), "Informação de Candidatura", new ImageIcon("info.png"));
            }

            @Override
            public void windowClosing(WindowEvent e) {
                terminar();
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }

        });

        setVisible(true);

    }

    /**
     * Termina a aplicação.
     */
    private void terminar() {
        String[] itens = {"Sim", "Não", "Cancelar"};
        int res = JOptionPane.showOptionDialog(InserirDecisaoDialogo.this,
                "Está prestes a fechar esta caixa de diálogo.\n"
                + "Deseja guardar os dados introduzidos?",
                "Fechar Caixa de Diálogo de Submissão de Candidatura?",
                0,
                JOptionPane.QUESTION_MESSAGE,
                new ImageIcon("question.png"),
                itens,
                itens[2]);

        final int SIM = 0;
        final int NAO = 1;
        if (res == SIM) {
            try {
                if (btnSim.isSelected()) {
                    registar(true);
                } else if (btnNao.isSelected()) {
                    registar(false);
                } else {
                    JOptionPane.showMessageDialog(InserirDecisaoDialogo.this, "Tem de selecionar uma opção de deferimento da candidatura para prosseguir!", "ATENÇÃO", JOptionPane.WARNING_MESSAGE, new ImageIcon("warning.png"));
                }
            } catch (IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(InserirDecisaoDialogo.this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE, new ImageIcon("error.png"));
            }
        } else if (res == NAO) {
            dispose();
        }
    }

    /**
     * Devolve o painel que é adicionado à região norte da JDialog.
     *
     * @param titulo o título da janela
     * @return o painel criado
     */
    private JPanel criarPainelNorte(String titulo) {
        JPanel painelNorte = new JPanel();
        painelNorte.setBackground(new Color(180, 202, 237));
        painelNorte.add(criarLabelNorte(titulo));

        return painelNorte;
    }

    /**
     * Devolve a label com o título da janela.
     *
     * @param titulo o título da janela
     * @return a labelc criada
     */
    private JLabel criarLabelNorte(String titulo) {
        JLabel lblNorte = new JLabel(titulo);

        lblNorte.setFont(new Font("Segoe UI", Font.ITALIC, 24));

        return lblNorte;
    }

    /**
     * Devolve o painel que é adicionado ao topo do painel central da JDialog.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCentroSuperior() {
        JPanel painelCentroSuperior = new JPanel();
        JLabel lblTitulo = new JLabel("Insira a Decisão");
        lblTitulo.setFont(new Font("Segoe UI", Font.PLAIN, 19));

        painelCentroSuperior.add(lblTitulo);

        return painelCentroSuperior;
    }

    /**
     * Devolve o painel que é adicionado à região centro da JDialog.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCentro() {

        JPanel painelCentro = new JPanel();
        painelCentro.setLayout(new BoxLayout(painelCentro, BoxLayout.Y_AXIS));

        painelCentro.add(criarPainelCentroSuperior());
        painelCentro.add(criarPainelLabelAceite());
        painelCentro.add(criarPainelJRadioButtons());
        painelCentro.add(criarPainelLabelTexto());
        painelCentro.add(criarPainelCampoTexto());

        return painelCentro;

    }

    /**
     * Devolve o painel que contém apenas a label "Aceite?"
     *
     * @return o painel criado
     */
    private JPanel criarPainelLabelAceite() {
        JLabel lblAceite = new JLabel("Aceite?");
        lblAceite.setFont(new Font("Segoe UI", Font.PLAIN, 15));
        JPanel painel = new JPanel();
        painel.add(lblAceite);

        return painel;
    }

    /**
     * Devolve o painel que contém os RadioButtons, correspondentes às opções "Sim" e "Não"
     *
     * @return o painel criado
     */
    private JPanel criarPainelJRadioButtons() {
        JPanel painelRadioButtons = new JPanel();
        FlowLayout fl = (FlowLayout) painelRadioButtons.getLayout();
        fl.setHgap(45);

        btnSim = new JRadioButton("Sim");
        btnNao = new JRadioButton("Não");

        painelRadioButtons.add(btnSim);
        painelRadioButtons.add(btnNao);
        agruparBotoes();

        return painelRadioButtons;

    }

    /**
     * Agrupa os RadioButtons, para que só um possa ser selecionado.
     */
    private void agruparBotoes() {
        ButtonGroup grupo = new ButtonGroup();
        grupo.add(btnSim);
        grupo.add(btnNao);
    }

    /**
     * Devolve o painel que contém a label "Texto Justificativo"
     *
     * @return o painel criado
     */
    private JPanel criarPainelLabelTexto() {
        JLabel lblTexto = new JLabel("Texto Justificativo:");
        lblTexto.setFont(new Font("Segoe UI", Font.PLAIN, 15));

        JPanel painel = new JPanel();

        painel.add(lblTexto);

        return painel;
    }

    /**
     * Devolve o painel que contém o campo de texto onde é inserido o texto justificativo da decisão.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCampoTexto() {
        JPanel painel = new JPanel();

        txtTextoJ = new JTextArea(10, 30);
        txtTextoJ.setLineWrap(true);

        Border border = BorderFactory.createLineBorder(Color.GRAY);
        txtTextoJ.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        JScrollPane scrPane = new JScrollPane(txtTextoJ);
        // criar uma linha à volta da TextArea, para não confundir com o background

        scrPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        scrPane.setBorder(
                BorderFactory.createTitledBorder("Texto Justificativo da Decisão"));

        painel.add(scrPane);

        return painel;
    }

    /**
     * Devolve o painel que é adicionado à região sul da JDialog.
     *
     * @return o painel criado
     */
    private JPanel criarPainelSul() {
        JPanel painelSul = new JPanel();
        painelSul.setBackground(new Color(18, 69, 150));
        FlowLayout fl = (FlowLayout) painelSul.getLayout();
        fl.setHgap(27);

        painelSul.add(criarBotaoRetroceder());
        painelSul.add(criarBotaoGuardar());
        painelSul.add(criarBotaoAnular());

        return painelSul;
    }

    /**
     * Cria e devolve o botão "Retroceder", que permite ao utilizador voltar para a página anterior. ícone do botão: Autor - Cursor Creative http://www.flaticon.com/authors/cursor-creative Disponível em: http://www.flaticon.com/free-icon/return_339736#term=back&page=4&position=83
     *
     * @return o botão criado
     */
    private JButton criarBotaoRetroceder() {
        JButton btnBack = new JButton("Retroceder");

        btnBack.setText("Retroceder");
        btnBack.setToolTipText("Volta para a página anterior.");
        btnBack.setMnemonic(KeyEvent.VK_R);
        btnBack.setIcon(new ImageIcon("return.png"));

        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                InserirDecisaoDialogo.this.dispose();
            }
        });

        return btnBack;
    }

    /**
     * Cria e devolve o botão "Guardar", que guarda a decisão no sistema e volta para o menu principal. ícone do botão: Autor - Freepik http://www.flaticon.com/authors/freepik Disponível em: http://www.flaticon.com/free-icon/diskette_167489#term=save&page=1&position=63
     *
     * @return o botão criado
     */
    private JButton criarBotaoGuardar() {
        JButton btnGuardar = new JButton("Guardar");
        btnGuardar.setToolTipText("Regista a decisão no sistema.");
        btnGuardar.setMnemonic(KeyEvent.VK_G);

        btnGuardar.setIcon(new ImageIcon("save.png"));

        btnGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                String[] itens2 = {"Sim", "Não"};

                int res = JOptionPane.showOptionDialog(InserirDecisaoDialogo.this, "Confirma os dados?", "Confirmação de Dados Introduzidos", 0, JOptionPane.QUESTION_MESSAGE, new ImageIcon("question.png"), itens2, itens2[1]);

                final int SIM = 0;
                if (res == SIM) {

                    try {
                        if (btnSim.isSelected()) {
                            registar(true);
                        } else if (btnNao.isSelected()) {

                            registar(false);
                        } else {
                            JOptionPane.showMessageDialog(InserirDecisaoDialogo.this, "Tem de selecionar uma opção de deferimento da candidatura para prosseguir!", "ATENÇÃO", JOptionPane.WARNING_MESSAGE, new ImageIcon("warning.png"));
                        }
                    } catch (IllegalArgumentException ex) {
                        JOptionPane.showMessageDialog(InserirDecisaoDialogo.this, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE, new ImageIcon("error.png"));
                    }

                }
            }
        });

        return btnGuardar;
    }

    /**
     * Cria o botão que anula os dados inseridos até ao momento. ícone do botão: Autor - MadeByOliver http://www.flaticon.com/authors/madebyoliver Disponível em: http://www.flaticon.com/free-icon/backspace_137523#term=backspace&page=1&position=1
     *
     * @return o botão "Anular" criado
     */
    private JButton criarBotaoAnular() {
        JButton btnAnular = new JButton("Anular");
        btnAnular.setToolTipText("Anula todos os dados inseridos.");
        btnAnular.setMnemonic(KeyEvent.VK_A);

        btnAnular.setIcon(new ImageIcon("delete.png"));

        btnAnular.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                btnSim.setSelected(false);
                btnNao.setSelected(false);
                txtTextoJ.setText(null);
            }
        });

        return btnAnular;
    }

    /**
     * Método que despoleta a ação de registo da candidatura no sistema
     * @param aceite o booleano que indica se a candidatura foi aceite ou não
     * @throws IllegalArgumentException a exceção que pode ser lançada caso o utilizador não introduza os dados de forma correta
     */
    private void registar(boolean aceite) throws IllegalArgumentException {
        guardarDecisao(true);
        JOptionPane.showMessageDialog(InserirDecisaoDialogo.this, "Dados guardados com sucesso!", "Sucesso na gravação dos dados", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("info.png"));
        dispose();
    }

    /**
     * Método que adiciona a decisão do FAE à lista de decisões da respetiva candidatura.
     *
     * @param decisao a decisão da candidatura (true - aceite, false - rejeitada)
     * @throws IllegalArgumentException a exceção que pode ser lançada ao tentar guardar uma decisão com um texto justificativo inválido
     */
    private void guardarDecisao(boolean decisao) throws IllegalArgumentException {
        cand.adicionarDecisao(new Decisao(decisao, txtTextoJ.getText()));
    }

}
