/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.CentroDeEventos;
import Model.Evento;
import com.sun.glass.events.KeyEvent;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A janela de diálogo que permite ao utilizador iniciar o processo de decisão de uma candidatura.
 * 
 * @author m
 */
public class DecidirCandidaturaDialogo extends JDialog {

    /**
     * O centro de eventos.
     */
    private CentroDeEventos ce;
    
    /**
     * O evento do qual se vai decidir uma candidatura.
     */
    private Evento evento;

    /**
     * A JComboBox com a lista de eventos a mostrar ao utilizador.
     */
    private JList listaEventos;
    
    /**
     * A Label que apresenta uma mensagem se o evento selecionado não possuir qualquer candidatura.
     */
    private JLabel lblInformacao;
    
    /**
     * O botão que abre a janela seguinte.
     */
    private JButton btnSeguinte;
    
    /**
     * A largura da janela (em pixeis).
     */
    private static final int LARGURA_JANELA = 900;
    
    /**
     * A altura da janela (em pixeis).
     */
    private static final int ALTURA_JANELA = 550;

    /**
     * Construtor que recebe por parâmetro o título da janela e o centro de eventos.
     * 
     * @param framePai
     * @param titulo o título da janela
     * @param ce o centro de eventos
     */
    public DecidirCandidaturaDialogo(MenuPrincipal framePai, String titulo, CentroDeEventos ce) {
        
        super(framePai);
        this.ce = ce;
        criarComponentes(titulo);
        iniciarDefinicoes();
    }
    
    /**
     * Cria os componentes da janela.
     * 
     * @param titulo o título da janela
     */
    private void criarComponentes(String titulo) {
        add(criarPainelNorte(titulo), BorderLayout.NORTH);
        add(criarPainelCentro(), BorderLayout.CENTER);
        add(criarPainelSul(), BorderLayout.SOUTH);
    }

    /**
     * Definições da janela.
     */
    private void iniciarDefinicoes() {
        setIconImage(new ImageIcon("calendar.png").getImage());
        setSize(LARGURA_JANELA, ALTURA_JANELA);
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setModal(true);
        setVisible(true);
    }

    /**
     * Devolve o painel que é adicionado à região norte da JDialog.
     * 
     * @param titulo o título da janela
     * @return o painel criado
     */
    private JPanel criarPainelNorte(String titulo) {
        JPanel painelNorte = new JPanel();
        painelNorte.setBackground(new Color(180, 202, 237));
        painelNorte.add(criarLabelNorte(titulo));

        return painelNorte;
    }
    
    /**
     * Devolve a label com o título da janela.
     * 
     * @param titulo o título da janela
     * @return a labelc criada
     */
    private JLabel criarLabelNorte(String titulo) {
        JLabel lblNorte = new JLabel(titulo);
        lblNorte.setFont(new Font("Segoe UI", Font.ITALIC, 24));
        lblNorte.setText(titulo);
        return lblNorte;
    }

    /**
     * Devolve a label com uma instrução para o utilizador.
     * 
     * @return a label criada
     */
    private JLabel criarLabelTitulo() {
        JLabel lblTitulo = new JLabel("Selecione o evento para o qual deseja decidir uma ou mais candidaturas");
        lblTitulo.setFont(new Font("Segoe UI", Font.PLAIN, 19));

        return lblTitulo;
    }

    /**
     * Devolve o painel que é adicionado ao topo do painel central da JDialog.
     * 
     * @return o painel criado
     */
    private JPanel criarPainelCentroSuperior() {
        JPanel painelCentroSuperior = new JPanel();

        painelCentroSuperior.add(criarLabelTitulo());

        return painelCentroSuperior;
    }

    /**
     * Devolve o painel que é adicionado à região centro da JDialog.
     * 
     * @return o painel criado
     */
    private JPanel criarPainelCentro() {
        JPanel painelCentro = new JPanel();
        painelCentro.setLayout(new BoxLayout(painelCentro, BoxLayout.Y_AXIS));

        painelCentro.add(criarPainelCentroSuperior());
        painelCentro.add(criarPainelListaEventos());
        painelCentro.add(criarPainelLabelInformacao());

        return painelCentro;

    }

    /**
     * Devolve o painel onde é mostrada a lista de eventos.
     * 
     * @return o painel criado
     */
    private JPanel criarPainelListaEventos() {
        JPanel painelListaEventos = new JPanel();
        painelListaEventos.add(criarListaEventos());

        return painelListaEventos;
    }

    /**
     * Devolve o JScrollPane onde se mostra a lista de eventos ao utilizador.
     * Código cellRenderer retirado de https://stackoverflow.com/questions/9560474/java-jlist-unwanted-tostring-conversion, autor Jeff Storey
     * @return o JScrollPane criada
     */
    private JScrollPane criarListaEventos() {

        ModeloRegistoEventos modeloRegistoEventos = new ModeloRegistoEventos(ce.getRegistoEventos());
        listaEventos = new JList(modeloRegistoEventos);

        class Renderer extends DefaultListCellRenderer {

            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                setText(String.format("%s  |  %s", ((Evento) value).getTitulo(), ((Evento) value).getSubFim().toDiaMesAnoString()));
                return c;
            }

        }

        listaEventos.setCellRenderer(new Renderer());

        listaEventos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JScrollPane scrPane = new JScrollPane(listaEventos);

        scrPane.setPreferredSize(new Dimension(400, 350));

        return scrPane;
    }
    
    /**
     * Devolve um painel com uma label informativa.
     * 
     * @return o painel criado
     */
    private JPanel criarPainelLabelInformacao() {
        lblInformacao = new JLabel("O evento selecionado não possui quaisquer candidaturas.");
        lblInformacao.setFont(new Font("Segoe UI", Font.BOLD, 15));
        lblInformacao.setVisible(false);

        JPanel painel = new JPanel();
        painel.add(lblInformacao);

        return painel;
    }

    /**
     * Devolve o painel que é adicionado à região sul da JDialog.
     * 
     * @return o painel criado
     */
    private JPanel criarPainelSul() {
        
        JPanel painelSul = new JPanel();
        painelSul.setBackground(new Color(18, 69, 150));
        FlowLayout fl = (FlowLayout) painelSul.getLayout();
        fl.setHgap(27);
        painelSul.add(criarBotaoRetroceder());
        painelSul.add(criarBtnSeguinte());

        return painelSul;
    }

    /**
     * Cria e devolve o botão "Retroceder", que permite ao utilizador voltar para a página anterior.
     * ícone do botão: Autor - Cursor Creative http://www.flaticon.com/authors/cursor-creative
     * Disponível em: http://www.flaticon.com/free-icon/return_339736#term=back&page=4&position=83
     * @return o botão criado
     */
    private JButton criarBotaoRetroceder() {
        JButton btnBack = new JButton();
        btnBack.setText("Retroceder");
        btnBack.setToolTipText("Volta para a página anterior.");
        btnBack.setMnemonic(KeyEvent.VK_R);
        btnBack.setIcon(new ImageIcon("return.png"));

        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DecidirCandidaturaDialogo.this.dispose();
            }
        });

        return btnBack;
    }

    /**
     * Cria e devolve o botão seguinte que abre a janela seguinte.
     * ícone do botão: Autor - Roundicons http://www.flaticon.com/authors/roundicons
     * Disponível em: http://www.flaticon.com/free-icon/next_189674#term=next&page=1&position=85
     * @return o botão criado
     */
    private JButton criarBtnSeguinte() {
        btnSeguinte = new JButton("Seguinte");
        btnSeguinte.setToolTipText("Avança para a página seguinte.");
        btnSeguinte.setMnemonic(KeyEvent.VK_S);
        btnSeguinte.setDisabledIcon(new ImageIcon("cancel.png"));
        btnSeguinte.setIcon(new ImageIcon("next.png"));

        btnSeguinte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                if (listaEventos.getSelectedValue() == null) {
                    JOptionPane.showMessageDialog(DecidirCandidaturaDialogo.this, "Tem de selecionar um evento para prosseguir!", "ATENÇÃO!", JOptionPane.WARNING_MESSAGE, new ImageIcon("warning.png"));
                } else {
                    new SelecionarCandidaturaDialogo(DecidirCandidaturaDialogo.this, "Decisão de Candidatura", ce, (Evento) listaEventos.getSelectedValue());
                }

            }
        });
        return btnSeguinte;
    }
}






    