/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.Evento;
import Model.RegistoEventos;
import javax.swing.AbstractListModel;

/**
 * O modelo do registo de eventos.
 * @author m
 */
public class ModeloRegistoEventos extends AbstractListModel {

    /**
     * O registo de eventos do centro de eventos.
     */
    private RegistoEventos registoEventos;
    
    /**
     * Construtor do modelo.
     * @param registoEventos o registo de eventos do centro de eventos
     */
    public ModeloRegistoEventos(RegistoEventos registoEventos) {
        this.registoEventos = registoEventos;
    }
    
    /**
     * Devolve o tamanho do registo de eventos
     * @return o tamanho do registo de eventos
     */
    @Override
    public int getSize() {
        return registoEventos.tamanho();
    }

    /**
     * Devolve o objeto situado na posição passada por parâmetro.
     * @param index o índice do objeto
     * @return o objeto
     */
    @Override
    public Object getElementAt(int index) {
        return registoEventos.obterEvento(index);
    }
    
    /**
     * Adiciona o evento passado por parâmetro ao registo de eventos do centro de eventos.
     * @param evento o evento a ser adicionados
     * @return true se foi bem sucedido, false caso contrário.
     */
    public boolean addElement(Evento evento){
        boolean eventoAdicionado = registoEventos.adicionarEvento(evento);
        if(eventoAdicionado)
            fireIntervalAdded(this, getSize()-1, getSize()-1);
        return eventoAdicionado;
    }
    
}
