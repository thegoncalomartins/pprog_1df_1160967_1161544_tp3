/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.Candidatura;
import Model.CentroDeEventos;
import Model.Evento;
import Model.Produto;
import Model.ProdutoJaExistenteException;
import com.sun.glass.events.KeyEvent;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * A janela de diálogo que permite ao utilizador inserir os dados de uma candidatura e submetê-la no sistema.
 * @author m
 */
public class InserirDadosCandidaturaDialogo extends JDialog {

    /**
     * O centro de eventos.
     */
    private CentroDeEventos ce;

    /**
     * O evento ao qual será submetida a candidatura.
     */
    private Evento evento;

    /**
     * A candidatura.
     */
    private Candidatura candidatura;

    /**
     * Campo de texto onde é introduzida a empresa.
     */
    private JTextField txtEmpresa;

    /**
     * Campo de texto onde é introduzida a morada.
     */
    private JTextField txtMorada;

    /**
     * Campo de texto onde é introduzido o nº de telemóvel.
     */
    private JTextField txtTelemovel;

    /**
     * Campo de texto onde são introduzidos os produtos.
     */
    private JTextField txtProdutos;

    /**
     * Campo de texto onde é introduzida a área.
     */
    private JTextField txtArea;

    /**
     * Campo de texto onde é introduzido o nº de convidados.
     */
    private JTextField txtConvidados;

    /**
     * O tamanho das labels do formulário.
     */
    private static final Dimension TAMANHO_LABELS_FORMULARIO = new JLabel("Produtos a expor (opcional):          ").getPreferredSize();

    /**
     * A fonte usada pelas labels e campos de texto do formulário.
     */
    private static final Font FONTE_FORMULARIO = new Font("Segoe UI", Font.PLAIN, 15);

    /**
     * O nº de colunas do formulário.
     */
    private static final int NUMERO_COLUNAS_FORMULARIO = 2;

    /**
     * O nº de linhas do formulário.
     */
    private static final int NUMERO_LINHAS_FORMULARIO = 6;

    /**
     * O nº de caracteres visíveis nos campos de texto.
     */
    private static final int NUMERO_CARACTERES_TEXT_FIELD = 15;

    /**
     * A largura da janela (em pixeis).
     */
    private static final int LARGURA_JANELA = 900;

    /**
     * A altura da janela (em pixeis).
     */
    private static final int ALTURA_JANELA = 550;

    /**
     * Construtor que recebe como parãmetro o título da janela, o centro de eventos, e o evento ao qual será submetida a candidatura.
     *
     * @param framePai
     * @param titulo o título da janela
     * @param ce o centro de eventos
     * @param evento o evento ao qual será submetida a candidatura
     */
    public InserirDadosCandidaturaDialogo(SubmeterCandidaturaDialogo framePai, String titulo, CentroDeEventos ce, Evento evento) {
        super(framePai);
        this.ce = ce;
        this.evento = evento;
        criarComponentes(titulo);
        iniciarDefinicoes();

    }

    /**
     * Definições da janela.
     */
    private void iniciarDefinicoes() {
        setIconImage(new ImageIcon("calendar.png").getImage());
        setSize(LARGURA_JANELA, ALTURA_JANELA);
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null);
        setModal(true);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                terminar();
            }
        });

        setVisible(true);
    }

    /**
     * Termina a aplicação.
     */
    private void terminar() {
        String[] itens = {"Sim", "Não", "Cancelar"};
        int res = JOptionPane.showOptionDialog(InserirDadosCandidaturaDialogo.this,
                "Está prestes a fechar esta caixa de diálogo.\n"
                + "Deseja guardar os dados introduzidos?",
                "Fechar Caixa de Diálogo de Submissão de Candidatura?",
                0,
                JOptionPane.QUESTION_MESSAGE,
                new ImageIcon("question.png"),
                itens,
                itens[2]);

        final int SIM = 0;
        final int NAO = 1;
        if (res == SIM) {
            try {
                candidatura = novaCandidatura();

                String[] itens2 = {"Sim", "Não"};

                int resposta = JOptionPane.showOptionDialog(InserirDadosCandidaturaDialogo.this, "Confirma os dados?", "Confirmação de Dados Introduzidos", 0, JOptionPane.QUESTION_MESSAGE, new ImageIcon("question.png"), itens2, itens2[1]);

                if (resposta == SIM) {
                    if (evento.getListaCandidaturas().registarCandidatura(candidatura)) {
                        JOptionPane.showMessageDialog(InserirDadosCandidaturaDialogo.this, "Os dados foram gravados com sucesso!", "Sucesso na gravação dos dados", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("info.png"));
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(InserirDadosCandidaturaDialogo.this, "Candidatura já existente.", "ATENÇÃO", JOptionPane.WARNING_MESSAGE, new ImageIcon("warning.png"));
                    }
                }
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(InserirDadosCandidaturaDialogo.this, "ERRO!\nVerifique os campos de texto de valores numéricos.\nUm ou mais estão inválidos.", "ERRO", JOptionPane.ERROR_MESSAGE, new ImageIcon("error.png"));
            } catch (ProdutoJaExistenteException e) {
                JOptionPane.showMessageDialog(InserirDadosCandidaturaDialogo.this, e.getMessage(), "ATENÇÃO", JOptionPane.WARNING_MESSAGE, new ImageIcon("warning.png"));
            } catch (IllegalArgumentException e) {
                JOptionPane.showMessageDialog(InserirDadosCandidaturaDialogo.this, e.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE, new ImageIcon("error.png"));
            }
        } else if (res == NAO) {
            dispose();
        }

    }

    /**
     * Cria os componentes da janela.
     *
     * @param titulo o título da janela
     */
    private void criarComponentes(String titulo) {

        add(criarPainelNorte(titulo), BorderLayout.NORTH);
        add(criarPainelCentro(), BorderLayout.CENTER);
        add(criarPainelSul(), BorderLayout.SOUTH);

    }

    /**
     * Devolve o painel que é adicionado à região norte da JDialog.
     *
     * @param titulo o título da janela
     * @return o painel criado
     */
    private JPanel criarPainelNorte(String titulo) {
        JPanel painelNorte = new JPanel();
        JLabel lblNorte = new JLabel(titulo);
        painelNorte.setBackground(new Color(180, 202, 237));
        lblNorte.setFont(new Font("Segoe UI", Font.ITALIC, 24));
        painelNorte.add(lblNorte);
        return painelNorte;
    }

    /**
     * Devolve o painel que é adicionado à região centro da JDialog.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCentro() {

        JPanel painelCentro = new JPanel();
        painelCentro.setLayout(new BoxLayout(painelCentro, BoxLayout.Y_AXIS));

        painelCentro.add(criarPainelCentroSuperior());

        painelCentro.add(criarPainelCentroCentro());

        return painelCentro;

    }

    /**
     * Devolve os paineis correspondentes à label e ao campo de texto da empresa.
     *
     * @return os paineis criados
     */
    private JPanel[] criarPaineisEmpresa() {

        JLabel lblEmpresa = new JLabel("Nome da Empresa:");
        lblEmpresa.setFont(FONTE_FORMULARIO);
        lblEmpresa.setPreferredSize(TAMANHO_LABELS_FORMULARIO);
        txtEmpresa = new JTextField(NUMERO_CARACTERES_TEXT_FIELD);
        txtEmpresa.setFont(FONTE_FORMULARIO);

        JPanel painelEmpresa1 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel painelEmpresa2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelEmpresa1.add(lblEmpresa);
        painelEmpresa2.add(txtEmpresa);

        JPanel[] paineis = {painelEmpresa1, painelEmpresa2};
        return paineis;
    }

    /**
     * Devolve os paineis correspondentes à label e ao campo de texto da morada.
     *
     * @return os paineis criados
     */
    private JPanel[] criarPaineisMorada() {

        JLabel lblMorada = new JLabel("Morada:");
        lblMorada.setFont(FONTE_FORMULARIO);
        lblMorada.setPreferredSize(TAMANHO_LABELS_FORMULARIO);
        txtMorada = new JTextField(NUMERO_CARACTERES_TEXT_FIELD);
        txtMorada.setFont(FONTE_FORMULARIO);

        JPanel painelMorada1 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel painelMorada2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelMorada1.add(lblMorada);
        painelMorada2.add(txtMorada);

        JPanel[] paineis = {painelMorada1, painelMorada2};
        return paineis;
    }

    /**
     * Devolve os paineis correspondentes à label e ao campo de texto do nº de telemóvel.
     *
     * @return os paineis criados
     */
    private JPanel[] criarPaineisTelemovel() {

        JLabel lblTelemovel = new JLabel("Número de Telemóvel:");
        lblTelemovel.setFont(FONTE_FORMULARIO);
        lblTelemovel.setPreferredSize(TAMANHO_LABELS_FORMULARIO);
        txtTelemovel = new JTextField(NUMERO_CARACTERES_TEXT_FIELD);
        txtTelemovel.setFont(FONTE_FORMULARIO);

        txtTelemovel.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(java.awt.event.KeyEvent ke) {

                char c = ke.getKeyChar();

                // se o caracter não for númerico, backspace ou delete, não aparece na TextField
                if (!(Character.isDigit(c) || c == KeyEvent.VK_BACKSPACE || c == KeyEvent.VK_DELETE)) {
                    // getToolkit().beep();
                    ke.consume();
                }

                if (((JTextField) ke.getSource()).getText().length() >= 9) {
                    // getToolkit().beep();
                    ke.consume();
                }
            }

            @Override
            public void keyPressed(java.awt.event.KeyEvent ke) {

            }

            @Override
            public void keyReleased(java.awt.event.KeyEvent e) {

            }

        });

        JPanel painelTelemovel1 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel painelTelemovel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelTelemovel1.add(lblTelemovel);
        painelTelemovel2.add(txtTelemovel);

        JPanel[] paineis = {painelTelemovel1, painelTelemovel2};
        return paineis;
    }

    /**
     * Devolve os paineis correspondentes à label e ao campo de texto da área.
     *
     * @return os paineis criados
     */
    private JPanel[] criarPaineisArea() {

        JLabel lblArea = new JLabel("Área de Exposição (m2):");
        lblArea.setFont(FONTE_FORMULARIO);
        lblArea.setPreferredSize(TAMANHO_LABELS_FORMULARIO);
        txtArea = new JTextField(NUMERO_CARACTERES_TEXT_FIELD);
        txtArea.setFont(FONTE_FORMULARIO);

        txtArea.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(java.awt.event.KeyEvent ke) {

                char c = ke.getKeyChar();

                // se o caracter não for númerico, backspace ou delete, não aparece na TextField
                if (!(Character.isDigit(c) || c == KeyEvent.VK_BACKSPACE || c == KeyEvent.VK_DELETE || c == KeyEvent.VK_COMMA || c == KeyEvent.VK_PERIOD)) {
                    // getToolkit().beep();
                    ke.consume();
                }
            }

            @Override
            public void keyPressed(java.awt.event.KeyEvent ke) {

            }

            @Override
            public void keyReleased(java.awt.event.KeyEvent e) {

            }

        });

        JPanel painelArea1 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel painelArea2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelArea1.add(lblArea);
        painelArea2.add(txtArea);

        JPanel[] paineis = {painelArea1, painelArea2};
        return paineis;
    }

    /**
     * Devolve os paineis correspondentes à label e ao campo de texto dos produtos.
     *
     * @return os paineis criados
     */
    private JPanel[] criarPaineisProdutos() {

        JLabel lblProdutos = new JLabel("Produtos a expor (opcional):");
        lblProdutos.setToolTipText("Separe os produtos e respetiva quantidade por ponto e vírgula (;) \n Exemplo: "
                + "Placa Gráfica;3;Smartphone;5");
        lblProdutos.setFont(FONTE_FORMULARIO);
        lblProdutos.setPreferredSize(TAMANHO_LABELS_FORMULARIO);

        txtProdutos = new JTextField(NUMERO_CARACTERES_TEXT_FIELD);
        txtProdutos.setFont(FONTE_FORMULARIO);

        JPanel painelProdutos1 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel painelProdutos2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelProdutos1.add(lblProdutos);
        painelProdutos2.add(txtProdutos);

        JPanel[] paineis = {painelProdutos1, painelProdutos2};
        return paineis;
    }

    /**
     * Devolve os paineis correspondentes à label e ao campo de texto dos convidados.
     *
     * @return os paineis criados
     */
    private JPanel[] criarPaineisConvidados() {

        JLabel lblConvidados = new JLabel("Número de Convidados:");
        lblConvidados.setFont(FONTE_FORMULARIO);
        lblConvidados.setPreferredSize(TAMANHO_LABELS_FORMULARIO);
        txtConvidados = new JTextField(NUMERO_CARACTERES_TEXT_FIELD);
        txtConvidados.setFont(FONTE_FORMULARIO);

        txtConvidados.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(java.awt.event.KeyEvent ke) {

                char c = ke.getKeyChar();

                // se o caracter não for númerico, backspace ou delete, não aparece na TextField
                if (!(Character.isDigit(c) || c == KeyEvent.VK_BACKSPACE || c == KeyEvent.VK_DELETE)) {
                    // getToolkit().beep();
                    ke.consume();
                }
            }

            @Override
            public void keyPressed(java.awt.event.KeyEvent ke) {

            }

            @Override
            public void keyReleased(java.awt.event.KeyEvent e) {

            }

        });

        JPanel painelConvidados1 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel painelConvidados2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelConvidados1.add(lblConvidados);
        painelConvidados2.add(txtConvidados);

        JPanel[] paineis = {painelConvidados1, painelConvidados2};
        return paineis;
    }

    /**
     * Devolve o painel que é adicionado ao centro do painel central da JDialog.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCentroCentro() {
        JPanel painelCentroC = new JPanel(new GridLayout(NUMERO_LINHAS_FORMULARIO, NUMERO_COLUNAS_FORMULARIO));

        painelCentroC.add(criarPaineisEmpresa()[0]);
        painelCentroC.add(criarPaineisEmpresa()[1]);

        painelCentroC.add(criarPaineisMorada()[0]);
        painelCentroC.add(criarPaineisMorada()[1]);

        painelCentroC.add(criarPaineisTelemovel()[0]);
        painelCentroC.add(criarPaineisTelemovel()[1]);

        painelCentroC.add(criarPaineisArea()[0]);
        painelCentroC.add(criarPaineisArea()[1]);

        painelCentroC.add(criarPaineisProdutos()[0]);
        painelCentroC.add(criarPaineisProdutos()[1]);

        painelCentroC.add(criarPaineisConvidados()[0]);
        painelCentroC.add(criarPaineisConvidados()[1]);

        return painelCentroC;
    }

    /**
     * Devolve a label com uma descrição do formulário.
     *
     * @return a label criada
     */
    private JLabel criarLabelTitulo() {
        JLabel lblTitulo = new JLabel("Formulário de Submissão de Nova Candidatura");
        lblTitulo.setFont(new Font("Segoe UI", Font.PLAIN, 19));
        return lblTitulo;
    }

    /**
     * Devolve o painel que é adicionado ao topo do painel central da JDialog.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCentroSuperior() {
        JPanel painel = new JPanel();

        painel.add(criarLabelTitulo());

        return painel;
    }

    /**
     * Devolve o painel que é adicionado à região sul da JDialog.
     *
     * @return o painel criado
     */
    private JPanel criarPainelSul() {
        JPanel painelSul = new JPanel();
        FlowLayout fl = (FlowLayout) painelSul.getLayout();
        fl.setHgap(27);
        painelSul.setBackground(new Color(18, 69, 150));
        painelSul.add(criarBtnRetroceder());
        painelSul.add(criarBtnGuardar());
        painelSul.add(criarBtnLimpar());
        return painelSul;
    }

    /**
     * Cria e devolve o botão "Retroceder", que permite ao utilizador voltar para a página anterior. ícone do botão: Autor - Cursor Creative http://www.flaticon.com/authors/cursor-creative Disponível em: http://www.flaticon.com/free-icon/return_339736#term=back&page=4&position=83
     *
     * @return o botão criado
     */
    private JButton criarBtnRetroceder() {
        JButton btnBack = new JButton();

        btnBack.setText("Retroceder");
        btnBack.setToolTipText("Volta para a página anterior.");
        btnBack.setMnemonic(KeyEvent.VK_R);
        btnBack.setIcon(new ImageIcon("return.png"));

        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                InserirDadosCandidaturaDialogo.this.dispose();
            }
        });

        return btnBack;
    }

    /**
     * Cria e devolve o botão "Guardar", que guarda a decisão no sistema e volta para o menu principal. ícone do botão: Autor - Freepik http://www.flaticon.com/authors/freepik Disponível em: http://www.flaticon.com/free-icon/diskette_167489#term=save&page=1&position=63
     *
     * @return o botão criado
     */
    private JButton criarBtnGuardar() {
        JButton btnGuardar = new JButton("Guardar");
        btnGuardar.setToolTipText("Regista a candidatura no sistema.");
        btnGuardar.setMnemonic(KeyEvent.VK_G);

        btnGuardar.setIcon(new ImageIcon("save.png"));

        btnGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                try {
                    candidatura = novaCandidatura();

                    String[] itens2 = {"Sim", "Não"};

                    int resposta = JOptionPane.showOptionDialog(InserirDadosCandidaturaDialogo.this, "Confirma os dados?", "Confirmação de Dados Introduzidos", 0, JOptionPane.QUESTION_MESSAGE, new ImageIcon("question.png"), itens2, itens2[1]);

                    final int SIM = 0;
                    if (resposta == SIM) {
                        if (evento.getListaCandidaturas().registarCandidatura(candidatura)) {
                            JOptionPane.showMessageDialog(InserirDadosCandidaturaDialogo.this, "Os dados foram gravados com sucesso!", "Sucesso na gravação dos dados", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("info.png"));
                            dispose();
                        } else {
                            JOptionPane.showMessageDialog(InserirDadosCandidaturaDialogo.this, "Candidatura já existente.", "ATENÇÃO", JOptionPane.WARNING_MESSAGE, new ImageIcon("warning.png"));
                        }
                    }
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(InserirDadosCandidaturaDialogo.this, "ERRO!\nVerifique os campos de texto de valores numéricos.\nUm ou mais estão inválidos.", "ERRO", JOptionPane.ERROR_MESSAGE, new ImageIcon("error.png"));
                } catch (ProdutoJaExistenteException e) {
                    JOptionPane.showMessageDialog(InserirDadosCandidaturaDialogo.this, e.getMessage(), "ATENÇÃO", JOptionPane.WARNING_MESSAGE, new ImageIcon("warning.png"));
                } catch (IllegalArgumentException e) {
                    JOptionPane.showMessageDialog(InserirDadosCandidaturaDialogo.this, e.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE, new ImageIcon("error.png"));
                }
            }
        }
        );
        return btnGuardar;
    }

    /**
     * Cria o botão que limpa os dados inseridos até ao momento. ícone do botão: Autor - MadeByOliver http://www.flaticon.com/authors/madebyoliver Disponível em: http://www.flaticon.com/free-icon/backspace_137523#term=backspace&page=1&position=1
     *
     * @return o botão "Limpar" criado
     */
    private JButton criarBtnLimpar() {
        JButton btnLimpar = new JButton("Limpar");
        btnLimpar.setToolTipText("Limpa todas as caixas de texto.");
        btnLimpar.setMnemonic(KeyEvent.VK_L);

        btnLimpar.setIcon(new ImageIcon("delete.png"));

        btnLimpar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                txtEmpresa.setText(null);
                txtMorada.setText(null);
                txtTelemovel.setText(null);
                txtProdutos.setText(null);
                txtArea.setText(null);
                txtConvidados.setText(null);
            }
        }
        );

        return btnLimpar;
    }

    /**
     * Cria e devolve uma nova candidatura.
     *
     * @return a candidatura criada
     * @throws NumberFormatException quando o utilizador não introduziu qualquer valor numérico nos campos de texto que o pedem
     * @throws ProdutoJaExistenteException exceção que pode ser lançada quando o utilizador tenta adicionar um produto já existente na lista
     */
    public Candidatura novaCandidatura() throws NumberFormatException, ProdutoJaExistenteException {

        String nomeEmpresa = txtEmpresa.getText();
        String morada = txtMorada.getText();
        String numeroTelemovel = txtTelemovel.getText();
        float areaExposicao = lerArea();
        int numeroConvidados = Integer.parseInt(txtConvidados.getText());

        Candidatura c = evento.getListaCandidaturas().novaCandidatura(nomeEmpresa, morada, numeroTelemovel, areaExposicao, numeroConvidados);

        if (txtProdutos.getText().length() != 0) {
            if (txtProdutos.getText().contains(";")) {
                String[] produtos = txtProdutos.getText().split(";");
                if (produtos.length % 2 != 0) {
                    throw new IllegalArgumentException("Formato dos produtos a expor é inválido!\n"
                            + "Passe o cursor por cima da Label para mais informações.");
                } else {
                    for (int i = 0; i < produtos.length; i += 2) {
                        String nomeProduto = produtos[i];
                        int quantidade = Integer.parseInt(produtos[i + 1]);
                        c.adicionarProdutoAExpor(new Produto(nomeProduto, quantidade));
                    }
                }
            } else {
                throw new IllegalArgumentException("Formato dos produtos a expor é inválido!\n"
                        + "Passe o cursor por cima da Label para mais informações.");
            }
        }

        return c;
    }

    /**
     * Método que permite ao utilizador introduzir um valor inteiro ou decimal com ',' ou '.' no campo de texto da área de exposição A vantagem deste método é que o utilizador decide se quer colocar um valor decimal com separador "," ou "." Exemplo: 2,4 ou 2.4 , Ambos são válidos e aceites.
     *
     * @return o valor da área introduzida no campo de texto da Área de Exposição
     */
    private float lerArea() throws NumberFormatException {

        float areaExp;

        if (contemCaracterUmaSoVez(txtArea.getText(), ',')) {
            String[] vArea = txtArea.getText().split(",");

            if (vArea.length != 2) {
                throw new IllegalArgumentException("O valor da área introduzido é inválido!");
            }

            int[] area = new int[2];

            for (int i = 0; i < vArea.length; i++) {
                area[i] = Integer.parseInt(vArea[i]);
            }

            areaExp = area[0] + Float.parseFloat("0." + area[1]);
        } else {

            areaExp = Float.parseFloat(txtArea.getText());
        }

        return areaExp;
    }

    /**
     * Averigua se a string passada por parâmetro contém uma só vez um caracter também passado por parâmetro
     *
     * @param a a string recebida
     * @param b o caracter a ser verificado
     * @return true se a string contém o caracter b uma só vez
     */
    private boolean contemCaracterUmaSoVez(String a, char b) {

        int cont = 0;

        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) == b) {
                cont++;
            }

        }

        return cont == 1;
    }

}
