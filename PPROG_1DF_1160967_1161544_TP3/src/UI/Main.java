/* 
Instituição de Ensino Superior : ISEP - Instituto Superior de Engenharia do Porto
Ano Académico - 2016/2017
Unidade Curricular : Paradigmas da Programação (PPROG)
Docente : Fernando Jorge Duarte (FJD)
Autores : 1160967, João Dias e 1161544, Gonçalo Martins
Turma - 1DF
Projeto entregue no dia 27/05/2017

Todos os ícones usados no desenvolvimento desta aplicação
estão disponíveis em:	flaticon.com

Autores:
http://www.flaticon.com/authors/dimitry-miroliubov - Dimitry Miroliubov

http://www.flaticon.com/authors/cursor-creative - Cursor Creative

http://www.flaticon.com/authors/madebyoliver - Madebyoliver

http://www.flaticon.com/authors/vectors-market - Vectors Market

http://www.flaticon.com/authors/prosymbols - Prosymbols

http://www.flaticon.com/authors/freepik - Freepik

http://www.flaticon.com/authors/roundicons - Roundicons

FORAM UTILIZADOS APENAS PARA UM PROJETO/TRABALHO ACADÉMICO, NÃO RECEBEMOS QUALQUER BENEFÍCIO MONETÁRIO COM
A UTILIZAÇÃO DESTES ÍCONES.

Código de outras fontes utilizado:
Código cellRenderer retirado de https://stackoverflow.com/questions/9560474/java-jlist-unwanted-tostring-conversion, autor Jeff Storey
Este código foi utilizado com o intuito de permitir que as células das JList não ficassem preenchidas com o toString() dos objetos, que era bastante longo.
Permitiu-nos escolher quais as informações que pretendíamos que as células mostrassem, como por exemplo o nome ou outro atributo relevante.
*/

package UI;

import Model.AlgoritmoAtribuicaoCargaEquitativa;
import Model.AlgoritmoAtribuicaoExperienciaProfissional;
import Model.AlgoritmoAtribuicaoNFAECandidatura;
import Model.CentroDeEventos;
import Model.FicheiroCentroEventos;
import java.io.FileNotFoundException;

/**
 * A classe Main, responsável por permitir a execução da aplicação.
 * @author Gonçalo Martins
 */
public class Main {

    /**
     * Método main, o responsável por arrancar a aplicação.
     * @param args os argumentos da linha de comandos
     * @throws FileNotFoundException exceção que pode ser lançada ao ler um ficheiro de texto
     */
    public static void main(String[] args) throws FileNotFoundException {

        FicheiroCentroEventos ficheiroCentroEventos = new FicheiroCentroEventos();

        CentroDeEventos ce = ficheiroCentroEventos.ler(FicheiroCentroEventos.NOME_FICHEIRO_BINARIO);

        if (ce == null) {
            ce = new CentroDeEventos();
            ce.getRegistoAlgoritmos().adicionarAlgoritmo(new AlgoritmoAtribuicaoCargaEquitativa());
            ce.getRegistoAlgoritmos().adicionarAlgoritmo(new AlgoritmoAtribuicaoNFAECandidatura());
            ce.getRegistoAlgoritmos().adicionarAlgoritmo(new AlgoritmoAtribuicaoExperienciaProfissional());
            ficheiroCentroEventos.lerFicheiroTexto(FicheiroCentroEventos.NOME_FICHEIRO_TEXTO, ce);
        }
        
        new MenuPrincipal("CentroDeEventos", ce, ficheiroCentroEventos);
    }

}
