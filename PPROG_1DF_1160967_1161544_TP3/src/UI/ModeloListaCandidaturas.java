/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.Candidatura;
import Model.ListaCandidaturas;
import javax.swing.AbstractListModel;

/**
 * O modelo da lista de candidaturas.
 * 
 * @author Gonçalo Martins
 */
public class ModeloListaCandidaturas extends AbstractListModel {

    /**
     * A lista de candidaturas.
     */
    private ListaCandidaturas listaCandidaturas;

    /**
     * Construtor do modelo.
     * @param listaCandidaturas a lista de candidaturas do evento
     */
    public ModeloListaCandidaturas(ListaCandidaturas listaCandidaturas) {
        this.listaCandidaturas = listaCandidaturas;
    }

    /**
     * Devolve o tamanho da lista de candidaturas.
     * 
     * @return o tamanho da lista de candidaturas
     */
    @Override
    public int getSize() {
        return listaCandidaturas.tamanho();
    }

    /**
     * Devolve o objeto situado na posição passada por parâmetro.
     * @param index o índice do objeto
     * @return o objeto
     */
    @Override
    public Object getElementAt(int index) {
        return listaCandidaturas.obterCandidatura(index);
    }
    
    /**
     * Adiciona a candidatura passada por parâmetro à lista de candidaturas.
     * @param candidatura a candidatura a adicionar
     * @return true se conseguiu, false caso contrário.
     */
    public boolean addElement(Candidatura candidatura) {
        boolean candidaturaAdicionada = listaCandidaturas.registarCandidatura(candidatura);
        if(candidaturaAdicionada)
            fireIntervalAdded(this, getSize()-1, getSize()-1);
        return candidaturaAdicionada;
    }

}
