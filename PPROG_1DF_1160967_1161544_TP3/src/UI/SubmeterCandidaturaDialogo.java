/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.CentroDeEventos;
import Model.Evento;
import com.sun.glass.events.KeyEvent;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A janela de diálogo que permite ao utilizador (Representante) escolher o evento para o qual deseja participar.
 * 
 * @author m
 */
public class SubmeterCandidaturaDialogo extends JDialog {

    /**
     * O centro de eventos.
     */
    private CentroDeEventos ce;

    /**
     * A JList com a lista de eventos a mostrar ao utilizador.
     */
    private JList listaEventos;

    /**
     * O botão que abre a janela de introdução dos dados da candidatura.
     */
    private JButton btnSeguinte;

    /**
     * A largura da janela (em pixeis).
     */
    private static final int LARGURA_JANELA = 900;

    /**
     * A altura da janela (em pixeis).
     */
    private static final int ALTURA_JANELA = 550;

    /**
     * Construtor que recebe por parâmetro o componente pai, o título da janela e o centro de eventos.
     *
     * @param framePai a janela que chama este diálogo
     * @param titulo o título da janela
     * @param ce o centro de eventos
     */
    public SubmeterCandidaturaDialogo(MenuPrincipal framePai, String titulo, CentroDeEventos ce) {
        super(framePai);
        this.ce = ce;
        criarComponentes(titulo);
        iniciarDefinicoes();
    }

    /**
     * Definições da janela.
     */
    private void iniciarDefinicoes() {
        setIconImage(new ImageIcon("calendar.png").getImage());
        setSize(LARGURA_JANELA, ALTURA_JANELA);
        setMinimumSize(new Dimension(getWidth(), getHeight()));

        setModal(true);
        setLocationRelativeTo(null);
        setVisible(true);

    }

    /**
     * Cria os componentes da janela.
     *
     * @param titulo o título da janela
     */
    private void criarComponentes(String titulo) {

        add(criarPainelNorte(titulo), BorderLayout.NORTH);
        add(criarPainelSul(), BorderLayout.SOUTH);
        add(criarPainelCentro(), BorderLayout.CENTER);

    }

    /**
     * Devolve o painel que é adicionado à região norte da JDialog.
     *
     * @param titulo o título da janela
     * @return o painel criado
     */
    private JPanel criarPainelNorte(String titulo) {
        JPanel painelNorte = new JPanel();
        JLabel lblNorte = new JLabel(titulo);
        painelNorte.setBackground(new Color(180, 202, 237));
        lblNorte.setFont(new Font("Segoe UI", Font.ITALIC, 24));
        painelNorte.add(lblNorte);
        return painelNorte;
    }

    /**
     * Devolve o painel que é adicionado ao topo do painel central da JDialog.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCentroSuperior() {
        JPanel painelCentroSuperior = new JPanel();

        painelCentroSuperior.add(criarLabelTitulo());
        return painelCentroSuperior;
    }

    /**
     * Devolve o painel que é adicionado à região centro da JDialog.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCentro() {

        JPanel painelCentro = new JPanel();

        painelCentro.setLayout(new BoxLayout(painelCentro, BoxLayout.Y_AXIS));

        painelCentro.add(criarPainelCentroSuperior());
        JPanel p = new JPanel();
        JLabel lbl = new JLabel("Título do evento  |  Prazo para a submissão de candidaturas");
        p.add(lbl);
        painelCentro.add(p);
        painelCentro.add(criarPainelListaEventos());

        return painelCentro;

    }

    /**
     * Devolve o painel onde é mostrada a lista de eventos.
     *
     * @return o painel criado
     */
    private JPanel criarPainelListaEventos() {
        JPanel painelListaEv = new JPanel();

        painelListaEv.add(criarListaEventos());

        return painelListaEv;
    }

    /**
     * Devolve o JScrollPane onde se mostra a lista de eventos ao utilizador.
     * Código cellRenderer retirado de https://stackoverflow.com/questions/9560474/java-jlist-unwanted-tostring-conversion, autor Jeff Storey
     * @return o JScrollPane criado
     */
    private JScrollPane criarListaEventos() {

        ModeloRegistoEventos modeloRegistoEventos = new ModeloRegistoEventos(ce.getRegistoEventos());
        listaEventos = new JList(modeloRegistoEventos);

        class Renderer extends DefaultListCellRenderer {

            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                setText(String.format("%s  |  %s", ((Evento) value).getTitulo(), ((Evento) value).getSubFim().toDiaMesAnoString()));
                return c;
            }

        }

        listaEventos.setCellRenderer(new Renderer());

        listaEventos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JScrollPane scrPane = new JScrollPane(listaEventos);

        
        
        scrPane.setPreferredSize(new Dimension(400, 350));

        return scrPane;
    }

    /**
     * Devolve a label com uma instrução para o utilizador.
     *
     * @return a label criada
     */
    private JLabel criarLabelTitulo() {
        JLabel lblTitulo = new JLabel("Selecione o evento para o qual pretende elaborar uma candidatura");
        lblTitulo.setFont(new Font("Segoe UI", Font.PLAIN, 19));
        return lblTitulo;
    }

    /**
     * Devolve o painel que é adicionado à região sul da JDialog.
     *
     * @return o painel criado
     */
    private JPanel criarPainelSul() {
        JPanel painelSul = new JPanel();
        painelSul.setBackground(new Color(18, 69, 150));
        FlowLayout fl = (FlowLayout) painelSul.getLayout();
        fl.setHgap(27);
        painelSul.add(criarBtnRetroceder());
        painelSul.add(criarBtnSeguinte());
        return painelSul;
    }

    /**
     * Devolve o botão retroceder que fecha a janela atual e abre a janela anterior.
     *
     * @return o botão criado
     */
    private JButton criarBtnRetroceder() {
        JButton btnBack = new JButton();

        btnBack.setText("Retroceder");
        btnBack.setToolTipText("Volta para a página anterior.");
        btnBack.setMnemonic(KeyEvent.VK_R);
        btnBack.setIcon(new ImageIcon("return.png"));

        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SubmeterCandidaturaDialogo.this.dispose();
            }
        });

        return btnBack;
    }

    /**
     * Devolve o botão seguinte que abre a janela de introdução dos dados da candidatura.
     * 
     * @return o botão criado
     */
    private JButton criarBtnSeguinte() {
        btnSeguinte = new JButton("Seguinte");
        btnSeguinte.setToolTipText("Avança para a página seguinte.");
        btnSeguinte.setMnemonic(KeyEvent.VK_S);

        btnSeguinte.setIcon(new ImageIcon("next.png"));

        btnSeguinte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                if (listaEventos.getSelectedValue() == null) {
                    JOptionPane.showMessageDialog(SubmeterCandidaturaDialogo.this, "Tem de selecionar um evento para prosseguir!", "ATENÇÃO!", JOptionPane.WARNING_MESSAGE, new ImageIcon("warning.png"));
                } else {
                    new InserirDadosCandidaturaDialogo(SubmeterCandidaturaDialogo.this, "Submissão de Candidatura", ce, (Evento) listaEventos.getSelectedValue());
                }

            }
        });
        return btnSeguinte;
    }

}
