/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.CentroDeEventos;
import Model.Evento;
import com.sun.glass.events.KeyEvent;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A janela de diálogo que permite ao utilizador iniciar o processo de atribuição de candidaturas.
 * 
 * @author m
 */
public class AtribuirCandidaturaDialogo extends JDialog {

    /**
     * O centro de eventos.
     */
    private CentroDeEventos ce;

    /**
     * A JList que irá apresentar os eventos disponíveis.
     */
    private JList listaEventosOrg;

    /**
     * A Label que apresenta uma mensagem se o evento selecionado não possuir qualquer candidatura.
     */
    private JLabel lblInformacao;

    /**
     * O botão que permite avançar para a próxima página do Caso de Uso.
     */
    private JButton btnSeguinte;

    /**
     * A largura da janela (em pixeis).
     */
    private static final int LARGURA_JANELA = 900;

    /**
     * A altura da janela (em pixeis).
     */
    private static final int ALTURA_JANELA = 550;

    /**
     * Constrói uma janela correspondente ao caso de uso 3 - Atribuição de Candidaturas aos FAE.
     *
     * @param framePai
     * @param titulo o título da janela
     * @param ce o centro de eventos
     */
    public AtribuirCandidaturaDialogo(MenuPrincipal framePai, String titulo, CentroDeEventos ce) {
        super(framePai);

        this.ce = ce;

        criarComponentes(titulo);

        iniciarDefinicoes();

    }

    /**
     * Cria os componentes da janela.
     * 
     * @param titulo o título da janela
     */
    private void criarComponentes(String titulo) {
        add(criarPainelNorte(titulo), BorderLayout.NORTH);
        add(criarPainelSul(), BorderLayout.SOUTH);
        add(criarPainelCentro(), BorderLayout.CENTER);
    }

    /**
     * As definições da janela.
     */
    private void iniciarDefinicoes() {
        ImageIcon img = new ImageIcon("calendar.png");
        setIconImage(img.getImage());
        setSize(LARGURA_JANELA, ALTURA_JANELA);
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setModal(true);
        setVisible(true);
    }

    /**
     * Cria e devolve o painel que irá estar na região norte da janela.
     * @param titulo o título da janela a ser adicionado ao painel 
     * @return o painel criado
     */
    private JPanel criarPainelNorte(String titulo) {
        JPanel painelNorte = new JPanel();
        painelNorte.setBackground(new Color(180, 202, 237));
        painelNorte.add(criarLabelNorte(titulo));

        return painelNorte;
    }

    /**
     * Cria e devolve uma label com uma instrução para o utilizador.
     * @param titulo o texto de instrução a ser apresentado ao utilizador
     * @return a label criada
     */
    private JLabel criarLabelNorte(String titulo) {
        JLabel lblNorte = new JLabel(titulo);
        lblNorte.setFont(new Font("Segoe UI", Font.ITALIC, 24));
        return lblNorte;
    }

    /**
     * Cria e devolve o painel que irá ser adicionado ao topo da região centro da janela.
     * 
     * @return o painel criado
     */
    private JPanel criarPainelCentroSuperior() {
        JLabel lblTitulo = new JLabel("Selecione o evento para o qual deseja atribuir candidaturas aos FAE");
        lblTitulo.setFont(new Font("Segoe UI", Font.PLAIN, 19));

        JPanel painelCentroSuperior = new JPanel();
        painelCentroSuperior.add(lblTitulo);

        return painelCentroSuperior;
    }

    /**
     * Cria e devolve o painel que é adicionado à região centro da janela.
     * 
     * @return o painel criado
     */
    private JPanel criarPainelCentro() {
        JPanel painelCentro = new JPanel();
        painelCentro.setLayout(new BoxLayout(painelCentro, BoxLayout.Y_AXIS));

        painelCentro.add(criarPainelCentroSuperior());
        painelCentro.add(criarPainelListaEventosOrg());
        painelCentro.add(criarPainelLabelInformacao());
        return painelCentro;
    }

    /**
     * Cria e devolve o painel que contém a label com informação sobre as candidaturas do evento selecionado anteriormente.
     * 
     * @return o painel criado
     */
    private JPanel criarPainelLabelInformacao() {
        lblInformacao = new JLabel("O evento selecionado não possui quaisquer candidaturas.");
        lblInformacao.setFont(new Font("Segoe UI", Font.BOLD, 15));
        lblInformacao.setVisible(false);

        JPanel painel = new JPanel();
        painel.add(lblInformacao);

        return painel;
    }

    /**
     * Cria e devolve o painel que contém a lista de eventos do Organizador.
     * 
     * @return o painel criado
     */
    private JPanel criarPainelListaEventosOrg() {
        JPanel painelListaEventosOrg = new JPanel();
        painelListaEventosOrg.add(criarListaEventosOrg());

        return painelListaEventosOrg;
    }

    /**
     * Cria a lista visual que irá apresentar todos os eventos cujo utilizador é um organizador.
     * Código cellRenderer retirado de https://stackoverflow.com/questions/9560474/java-jlist-unwanted-tostring-conversion, autor Jeff Storey
     * @return a lista criada
     */
    private JScrollPane criarListaEventosOrg() {

        ModeloRegistoEventos modeloRegistoEventos = new ModeloRegistoEventos(ce.getRegistoEventos());
        listaEventosOrg = new JList(modeloRegistoEventos);

        class Renderer extends DefaultListCellRenderer {

            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                setText(String.format("%s  |  %s", ((Evento) value).getTitulo(), ((Evento) value).getSubFim().toDiaMesAnoString()));
                return c;
            }

        }

        listaEventosOrg.setCellRenderer(new Renderer());

        listaEventosOrg.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        JScrollPane scrPane = new JScrollPane(listaEventosOrg);

        scrPane.setPreferredSize(new Dimension(400, 350));

        return scrPane;
    }

    /**
     * Cria e devolve o painel que é adicionado à região Sul da janela (contém os botões "Retroceder" e "Seguinte").
     * 
     * @return o painel criado
     */
    private JPanel criarPainelSul() {
        JPanel painelSul = new JPanel();
        FlowLayout fl = (FlowLayout) painelSul.getLayout();
        fl.setHgap(27);
        painelSul.add(criarBotaoRetroceder());
        painelSul.add(criarBotaoSeguinte());
        painelSul.setBackground(new Color(18, 69, 150));

        return painelSul;
    }

    /**
     * Cria e devolve o botão "Retroceder", que permite ao utilizador voltar para a página anterior.
     * ícone do botão: Autor - Cursor Creative http://www.flaticon.com/authors/cursor-creative
     * Disponível em: http://www.flaticon.com/free-icon/return_339736#term=back&page=4&position=83
     * @return o botão criado
     */
    private JButton criarBotaoRetroceder() {
        JButton btnBack = new JButton();
        btnBack.setText("Retroceder");
        btnBack.setToolTipText("Volta para a página anterior.");
        btnBack.setMnemonic(KeyEvent.VK_R);
        btnBack.setIcon(new ImageIcon("return.png"));

        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AtribuirCandidaturaDialogo.this.dispose();
            }
        });

        return btnBack;
    }

    /**
     * Cria e devolve o botão seguinte que abre a janela seguinte.
     * ícone do botão: Autor - Roundicons http://www.flaticon.com/authors/roundicons
     * Disponível em: http://www.flaticon.com/free-icon/next_189674#term=next&page=1&position=85
     * @return o botão criado
     */
    private JButton criarBotaoSeguinte() {
        btnSeguinte = new JButton();
        btnSeguinte.setText("Seguinte");
        btnSeguinte.setToolTipText("Avança para a página seguinte.");
        btnSeguinte.setMnemonic(KeyEvent.VK_S);
        btnSeguinte.setIcon(new ImageIcon("next.png"));
        btnSeguinte.setDisabledIcon(new ImageIcon("cancel.png"));

        btnSeguinte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (listaEventosOrg.getSelectedValue() == null) {
                    JOptionPane.showMessageDialog(AtribuirCandidaturaDialogo.this, "Tem de selecionar um evento para prosseguir!", "ATENÇÃO!", JOptionPane.WARNING_MESSAGE, new ImageIcon("warning.png"));
                } else {
                    new SelecionarAlgoritmoDialogo(AtribuirCandidaturaDialogo.this, "Atribuição de Candidatura", ce, (Evento) listaEventosOrg.getSelectedValue());
                }

            }
        });

        return btnSeguinte;
    }
}
