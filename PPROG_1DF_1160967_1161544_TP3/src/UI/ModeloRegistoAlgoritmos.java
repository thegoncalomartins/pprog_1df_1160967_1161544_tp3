/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.AlgoritmoAtribuicao;
import Model.RegistoAlgoritmos;
import javax.swing.AbstractListModel;

/**
 * O modelo do registo de algoritmos.
 * @author Gonçalo Martins
 */
public class ModeloRegistoAlgoritmos extends AbstractListModel {

    /**
     * O registo de algoritmos do centro de algoritmos.
     */
    private RegistoAlgoritmos registoAlgoritmos;
    
    /**
     * Construtor do modelo.
     * @param registoAlgoritmos o registo de algoritmos do centro de algoritmos
     */
    public ModeloRegistoAlgoritmos(RegistoAlgoritmos registoAlgoritmos) {
        this.registoAlgoritmos = registoAlgoritmos;
    }
    
    /**
     * Devolve o tamanho do registo de algoritmos
     * @return o tamanho do registo de algoritmos
     */
    @Override
    public int getSize() {
        return registoAlgoritmos.tamanho();
    }

    /**
     * Devolve o objeto situado na posição passada por parâmetro.
     * @param index o índice do objeto
     * @return o objeto
     */
    @Override
    public Object getElementAt(int index) {
        return registoAlgoritmos.obterAlgoritmo(index);
    }
    
    /**
     * Adiciona o algoritmo passado por parâmetro ao registo de algoritmos do centro de algoritmos.
     * @param algoritmo o algoritmo a ser adicionado
     * @return true se foi bem sucedido, false caso contrário.
     */
    public boolean addElement(AlgoritmoAtribuicao algoritmo){
        boolean algoritmoAdicionado = registoAlgoritmos.adicionarAlgoritmo(algoritmo);
        if(algoritmoAdicionado)
            fireIntervalAdded(this, getSize()-1, getSize()-1);
        return algoritmoAdicionado;
    }
    
}
