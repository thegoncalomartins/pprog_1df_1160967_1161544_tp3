/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.CentroDeEventos;
import Model.FicheiroCentroEventos;
import com.sun.glass.events.KeyEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;

/**
 * A janela do Menu Principal, onde o utilizador pode selecionar o caso de uso que deseja correr.
 * 
 * @author Gonçalo Martins
 */
public class MenuPrincipal extends JFrame {

    /**
     * O centro de eventos.
     */
    private CentroDeEventos ce;

    /**
     * O ficheiro binário que irá conter o objeto do tipo CentroDeEventos.
     */
    private FicheiroCentroEventos ficheiroCentroEventos;

    /**
     * O tamanho dos botões referentes aos casos de uso.
     */
    private static final Dimension TAMANHO_BOTOES = new JButton("Submeter uma Candidatura", new ImageIcon("envelope.png")).getPreferredSize();

    /**
     * Constrói uma nova janela do menu da aplicação com um título passado por parâmetro.
     *
     * @param titulo titulo da janela
     * @param ce o centro de eventos
     * @param ficheiroCentroEventos
     */
    public MenuPrincipal(String titulo, CentroDeEventos ce, FicheiroCentroEventos ficheiroCentroEventos) {
        super(titulo);
        this.ce = ce;
        this.ficheiroCentroEventos = ficheiroCentroEventos;
        criarComponentes();

        iniciarDefinicoes();

    }

    /**
     * Cria os componentes da GUI do menu da aplicação.
     */
    private void criarComponentes() {

        add(criarPainelNorte(), BorderLayout.NORTH);
        add(criarPainelCentro(), BorderLayout.CENTER);
        add(criarPainelSul(), BorderLayout.SOUTH);

    }

    /**
     * As definições da janela.
     */
    private void iniciarDefinicoes() {
        ImageIcon img = new ImageIcon("calendar.png");
        setIconImage(img.getImage());

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                terminar();
            }
        });

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setSize(900, 550);
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Guarda os dados e termina a aplicação.
     */
    private void terminar() {
        String[] itens = {"Sim", "Não"};
        int res = JOptionPane.showOptionDialog(MenuPrincipal.this,
                "Tem a certeza de que pretende sair?",
                "Fechar Aplicação?",
                0,
                JOptionPane.QUESTION_MESSAGE,
                new ImageIcon("question.png"),
                itens,
                itens[1]);

        final int SIM = 0;
        if (res == SIM) {
            if (ficheiroCentroEventos.guardar(FicheiroCentroEventos.NOME_FICHEIRO_BINARIO, ce)) {
                JOptionPane.showMessageDialog(MenuPrincipal.this, "Os dados foram gravados com sucesso!", "Sucesso na gravação dos dados", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("info.png"));
            } else {
                JOptionPane.showMessageDialog(MenuPrincipal.this, "Ocorreu um problema na gravação dos dados", "ERRO", JOptionPane.ERROR_MESSAGE, new ImageIcon("error.png"));
            }
            System.exit(0);
        }

    }

    /**
     * Cria e devolve o painel que é adicionado à região Norte da janela (contém a label de boas vindas).
     *
     * @return o painel criado
     */
    private JPanel criarPainelNorte() {
        JLabel lblBemVindo = new JLabel("Bem-vindo, selecione uma opção!");

        lblBemVindo.setFont(new Font("Segoe UI", Font.ITALIC, 24));
        JPanel painelNorte = new JPanel();
        painelNorte.setBackground(new Color(180, 202, 237));
        painelNorte.add(lblBemVindo);
        return painelNorte;
    }

    /**
     * Devolve um painel com 3 botões correspondentes aos 3 casos de uso implementados, que irá ser adicionado à região centro da janela.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCentro() {

        JPanel painelCentro = new JPanel();
        painelCentro.setLayout(new BoxLayout(painelCentro, BoxLayout.Y_AXIS));

        JPanel painelAtribuir = new JPanel();
        painelAtribuir.add(criarBotaoAtribuirCand());
        painelCentro.add(painelAtribuir);

        JPanel painelDecidir = new JPanel();
        painelDecidir.add(criarBotaoDecidirCand());
        painelCentro.add(painelDecidir);

        JPanel painelSubmeter = new JPanel();
        painelSubmeter.add(criarBotaoSubmeterCand());
        painelCentro.add(painelSubmeter);

        return painelCentro;
    }

    /**
     * Devolve um painel com dois botões, sair e terminar sessão, que é adicionado à região Sul da janela.
     *
     * @return o painel criado
     */
    private JPanel criarPainelSul() {

        JPanel painelSul = new JPanel();
        FlowLayout fl = (FlowLayout) painelSul.getLayout();
        fl.setHgap(15);
        painelSul.setBackground(new Color(18, 69, 150));
        painelSul.add(criarBotaoSair());
        return painelSul;
    }

    /**
     * Cria e devolve o botão correspondente ao caso de uso 3. ícone do botão: Autor - Vectors Market http://www.flaticon.com/authors/vectors-market Disponível em: http://www.flaticon.com/free-icon/transfer_236807#term=connections&page=1&position=31
     *
     * @return o botão criado
     */
    private JButton criarBotaoAtribuirCand() {

        JButton atribuirCand = new JButton(new ImageIcon("transfer.png"));
        atribuirCand.setText("Atribuir candidatura(s)");
        atribuirCand.setToolTipText("Atribuição de uma ou mais candidaturas a um ou mais FAE (Funcionários de Apoio ao Evento), disponível para Organizadores");
        atribuirCand.setMnemonic(KeyEvent.VK_A);
        atribuirCand.setPreferredSize(TAMANHO_BOTOES);

        atribuirCand.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new AtribuirCandidaturaDialogo(MenuPrincipal.this, "Atribuição de Candidatura", ce);
            }
        });

        return atribuirCand;
    }

    /**
     * Cria e devolve o botão correspondente ao caso de uso 4. ícone do botão: Autor - Prosymbols http://www.flaticon.com/authors/prosymbols Disponível em: http://www.flaticon.com/free-icon/test_204278#term=test&page=1&position=5
     *
     * @return o botão criado
     */
    private JButton criarBotaoDecidirCand() {

        JButton decidirCand = new JButton(new ImageIcon("test.png"));
        decidirCand.setText("Decidir candidatura(s)");
        decidirCand.setToolTipText("Decidir uma ou mais candidaturas, disponível para FAE's (Funcionários de Apoio ao Evento)");
        decidirCand.setMnemonic(KeyEvent.VK_D);
        decidirCand.setPreferredSize(TAMANHO_BOTOES);

        decidirCand.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DecidirCandidaturaDialogo(MenuPrincipal.this, "Decisão de Candidatura", ce);
            }
        });

        return decidirCand;
    }

    /**
     * Cria e devolve o botão corresponde ao caso de uso 5. ícone do botão: Autor - Dimitry Miroliubov http://www.flaticon.com/authors/dimitry-miroliubov Disponível em: http://www.flaticon.com/free-icon/envelope_340212#term=envelope&page=6&position=25
     *
     * @return o botão criado
     */
    private JButton criarBotaoSubmeterCand() {

        JButton submeterCand = new JButton("Submeter uma candidatura");
        submeterCand.setIcon(new ImageIcon("envelope.png"));
        submeterCand.setToolTipText("Submeter uma candidatura a um Evento, disponível para Representantes de Participantes");
        submeterCand.setMnemonic(KeyEvent.VK_U);

        submeterCand.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SubmeterCandidaturaDialogo(MenuPrincipal.this, "Submissão de Candidatura", ce);
            }
        });

        return submeterCand;
    }

    /**
     * Cria e devolve o botão que termina a aplicação. ícone do botão: Autor - Freepik http://www.flaticon.com/authors/freepik Disponível em: http://www.flaticon.com/free-icon/turn-on_390140#term=off&page=3&position=51
     *
     * @return o botão criado
     */
    private JButton criarBotaoSair() {

        JButton btnSair = new JButton(new ImageIcon("close.png"));
        btnSair.setPreferredSize(new Dimension((int) TAMANHO_BOTOES.getWidth(), 36));
        btnSair.setText("Sair");
        btnSair.setToolTipText("Fecha a aplicação.");
        btnSair.setMnemonic(KeyEvent.VK_S);
        btnSair.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                terminar();
            }
        });
        return btnSair;
    }

}
