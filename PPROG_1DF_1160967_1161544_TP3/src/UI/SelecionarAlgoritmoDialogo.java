/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.AlgoritmoAtribuicao;
import Model.AlgoritmoAtribuicaoCargaEquitativa;
import Model.AlgoritmoAtribuicaoExperienciaProfissional;
import Model.AlgoritmoAtribuicaoNFAECandidatura;
import Model.Atribuicao;
import Model.CentroDeEventos;
import Model.Evento;
import com.sun.glass.events.KeyEvent;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.Border;

/**
 * Caixa de diálogo para o utilizador poder selecionar o mecanismo de atribuição pretendido.
 * @author Gonçalo Martins
 */
public class SelecionarAlgoritmoDialogo extends JDialog {

    /**
     * O centro de eventos.
     */
    private CentroDeEventos ce;

    /**
     * O evento selecionado anteriormente.
     */
    private Evento ev;

    /**
     * A lista responsável por mostrar os algoritmos disponíveis ao utilizador.
     */
    private JList listaAlgoritmos;

    /**
     * O campo de texto onde serão inseridos o número de FAE's pretendidos por candidatura.
     */
    private JTextField txtNFAE;

    /**
     * A largura da janela (em píxeis).
     */
    private static final int LARGURA_JANELA = 900;

    /**
     * A altura da janela (em píxeis).
     */
    private static final int ALTURA_JANELA = 550;

    /**
     * A largura da lista dos algoritmos (em píxeis).
     */
    private static final int LARGURA_LISTA_ALGORITMOS = 268;

    /**
     * A altura da lista dos algoritmos (em píxeis).
     */
    private static final int ALTURA_LISTA_ALGORITMOS = 190;

    /**
     * Cria uma nova janela de interface gráfica com o utilizador, para este selecionar o mecanismo pretendido.
     *
     * @param framePai o diálogo pai
     * @param titulo o título da janela
     * @param ce o centro de eventos
     * @param ev o evento selecionado anteriormente
     */
    public SelecionarAlgoritmoDialogo(AtribuirCandidaturaDialogo framePai, String titulo, CentroDeEventos ce, Evento ev) {
        super(framePai);
        this.ce = ce;
        this.ev = ev;

        criarComponentes(titulo);

        iniciarDefinicoes();

    }

    /**
     * Cria os componentes da janela.
     *
     * @param titulo o título da instrução
     */
    private void criarComponentes(String titulo) {
        add(criarPainelNorte(titulo), BorderLayout.NORTH);
        add(criarPainelCentro(), BorderLayout.CENTER);
        add(criarPainelSul(), BorderLayout.SOUTH);
    }

    /**
     * As definições da janela.
     */
    private void iniciarDefinicoes() {
        setIconImage(new ImageIcon("calendar.png").getImage());
        setSize(LARGURA_JANELA, ALTURA_JANELA);
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setModal(true);
        setVisible(true);
    }

    /**
     * Cria um painel que vai ser adicionado à região Norte da Janela.
     *
     * @param titulo o texto da instrução dada ao utilizador
     * @return o painel criado
     */
    private JPanel criarPainelNorte(String titulo) {
        JPanel painelNorte = new JPanel();
        JLabel lblNorte = new JLabel(titulo);
        painelNorte.setBackground(new Color(180, 202, 237));
        lblNorte.setFont(new Font("Segoe UI", Font.ITALIC, 24));
        painelNorte.add(lblNorte);
        return painelNorte;
    }

    /**
     * Cria uma painel que irá ser adicionado ao topo do painel central da janela.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCentroSuperior() {
        JLabel lblAlgoritmos = new JLabel("Selecione o mecanismo de atribuição de candidaturas aos FAE");
        lblAlgoritmos.setFont(new Font("Segoe UI", Font.PLAIN, 19));
        JPanel painelCentroSuperior = new JPanel();
        painelCentroSuperior.add(lblAlgoritmos);

        return painelCentroSuperior;
    }

    /**
     * Cria e devolve o painel que é adicionado à região centro da janela.
     *
     * @return o painel criado
     */
    private JPanel criarPainelCentro() {

        JPanel painelCentro = new JPanel();
        painelCentro.setLayout(new BoxLayout(painelCentro, BoxLayout.Y_AXIS));

        painelCentro.add(criarPainelCentroSuperior());
        painelCentro.add(criarPainelAlgoritmos());
        painelCentro.add(criarPainelNumeroFAEPretendidos());

        return painelCentro;
    }

    /**
     * Cria e devolve o painel que é adicionado à região sul da janela.
     *
     * @return o painel criado
     */
    private JPanel criarPainelSul() {
        JPanel painelSul = new JPanel();
        FlowLayout fl = (FlowLayout) painelSul.getLayout();
        fl.setHgap(27);
        painelSul.add(criarBotaoRetroceder());
        painelSul.add(criarBotaoGerar());
        painelSul.add(criarBotaoAjuda());
        painelSul.setBackground(new Color(18, 69, 150));

        return painelSul;
    }

    /**
     * Cria e devolve o painel que contém os radio buttons correspondentes aos diferentes mecanismos de atribuição de candidaturas.
     *
     * @return o painel criado
     */
    private JPanel criarPainelAlgoritmos() {
        JPanel painelAlgoritmos = new JPanel();
        painelAlgoritmos.add(criarListaAlgoritmosDisponiveis());
        return painelAlgoritmos;
    }

    /**
     * Cria e devolve a lista que contém os algoritmos de atribuição disponíveis.
     * Código cellRenderer retirado de https://stackoverflow.com/questions/9560474/java-jlist-unwanted-tostring-conversion, autor Jeff Storey
     * @return a lista de algoritmos disponíveis
     */
    private JScrollPane criarListaAlgoritmosDisponiveis() {
        ModeloRegistoAlgoritmos modeloRegistoAlgoritmos = new ModeloRegistoAlgoritmos(ce.getRegistoAlgoritmos());
        listaAlgoritmos = new JList(modeloRegistoAlgoritmos);

        class Renderer extends DefaultListCellRenderer {

            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                setText(String.format("%s", ((AlgoritmoAtribuicao) value)));
                return c;
            }

        }

        listaAlgoritmos.setCellRenderer(new Renderer());

        listaAlgoritmos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JScrollPane scrPane = new JScrollPane(listaAlgoritmos);
        
        scrPane.setBorder(
            BorderFactory.createTitledBorder("Mecanismos Disponíveis"));

        scrPane.setPreferredSize(new Dimension(LARGURA_LISTA_ALGORITMOS, ALTURA_LISTA_ALGORITMOS));

        return scrPane;
    }

    /**
     * Cria e devolve o painel que contém a label e o campo de texto onde o utilizador pode introduzir o número de FAE's pretendidos para cada candidatura. Caso o botão "Distribuição de Nº de FAE's por candidatura" esteja selecionado.
     *
     * @return o painel criado
     */
    private JPanel criarPainelNumeroFAEPretendidos() {
        JPanel painelNFAE = new JPanel();

        JLabel lblNFAE = new JLabel("Número de FAE's pretendidos por candidatura:");
        txtNFAE = new JTextField(2);

        txtNFAE.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(java.awt.event.KeyEvent ke) {

                char c = ke.getKeyChar();

                // se o caracter não for númerico, backspace ou delete, não aparece na TextField
                if (!(Character.isDigit(c) || c == KeyEvent.VK_BACKSPACE || c == KeyEvent.VK_DELETE)) {
                    // getToolkit().beep();
                    ke.consume();
                }
            }

            @Override
            public void keyPressed(java.awt.event.KeyEvent ke) {

            }

            @Override
            public void keyReleased(java.awt.event.KeyEvent e) {

            }

        });

        painelNFAE.add(lblNFAE);
        painelNFAE.add(txtNFAE);

        return painelNFAE;
    }

    /**
     * Cria e devolve o botão "Retroceder", que permite ao utilizador voltar para a página anterior.
     * ícone do botão: Autor - Cursor Creative http://www.flaticon.com/authors/cursor-creative
     * Disponível em: http://www.flaticon.com/free-icon/return_339736#term=back&page=4&position=83
     * @return o botão criado
     */
    private JButton criarBotaoRetroceder() {
        JButton btnBack = new JButton();

        btnBack.setText("Retroceder");
        btnBack.setToolTipText("Volta para a página anterior.");
        btnBack.setMnemonic(KeyEvent.VK_R);
        btnBack.setIcon(new ImageIcon("return.png"));

        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SelecionarAlgoritmoDialogo.this.dispose();
            }
        });

        return btnBack;
    }

    /**
     * Cria e devolve o botão "Gerar", que gera as atribuições entre os FAE's e as candidaturas, e após isso mostra uma caixa de diálogo ao utilizador com as devidas atribuições.
     * O utilizador pode continuar a gerar ou guardar as alterações como definitivas.
     * ícone do botão: Autor - MadeByOliver http://www.flaticon.com/authors/madebyoliver
     * Disponível em: http://www.flaticon.com/free-icon/repeat_148752#term=arrows&page=1&position=42
     * @return o botão criado
     */
    private JButton criarBotaoGerar() {
        JButton btnGerar = new JButton("Gerar Atribuições");
        btnGerar.setToolTipText("Gera atribuições entre FAE's e candidaturas, com base no mecanismo selecionado.");
        btnGerar.setMnemonic(KeyEvent.VK_G);

        btnGerar.setIcon(new ImageIcon("repeat.png"));

        btnGerar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                if (listaAlgoritmos.getSelectedValue() == null) {

                    JOptionPane.showMessageDialog(SelecionarAlgoritmoDialogo.this, "Tem de selecionar um mecanismo para prosseguir!", "ATENÇÃO!", JOptionPane.WARNING_MESSAGE, new ImageIcon("warning.png"));
                } else {
                    AlgoritmoAtribuicao alg = (AlgoritmoAtribuicao) listaAlgoritmos.getSelectedValue();

                    if (alg instanceof AlgoritmoAtribuicaoNFAECandidatura) {

                        try {

                            int nFAE = Integer.parseInt(txtNFAE.getText());
                            ((AlgoritmoAtribuicaoNFAECandidatura) alg).setNFAE(nFAE);
                            ArrayList<Atribuicao> atr = ((AlgoritmoAtribuicaoNFAECandidatura) alg).atribui(ev);
                            mostrarDialogoAtribuicoes(atr, alg); 
                        } catch (NumberFormatException ex) {
                            JOptionPane.showMessageDialog(SelecionarAlgoritmoDialogo.this, "Número de FAE's pretendidos é inválido!", "ERRO", JOptionPane.ERROR_MESSAGE, new ImageIcon("error.png"));
                        } catch (IllegalArgumentException e) {
                            JOptionPane.showMessageDialog(SelecionarAlgoritmoDialogo.this, e.getMessage(), "ATENÇÃO", JOptionPane.WARNING_MESSAGE, new ImageIcon("warning.png"));
                        }
                    } else if (alg instanceof AlgoritmoAtribuicaoCargaEquitativa) {
                        ArrayList<Atribuicao> atr = ((AlgoritmoAtribuicaoCargaEquitativa) alg).atribui(ev);
                        mostrarDialogoAtribuicoes(atr, alg);

                    } else {
                        ArrayList<Atribuicao> atr = ((AlgoritmoAtribuicaoExperienciaProfissional) alg).atribui(ev);
                        
                        mostrarDialogoAtribuicoes(atr, alg);
                    }
                }

            }
        });
        return btnGerar;
    }
    
    /**
     * Cria e devolve o botão "Ajuda", que disponibiliza um pequeno texto de ajuda para o utilizador.
     * @return o botão criado
     */
    private JButton criarBotaoAjuda() {
        JButton btnAjuda = new JButton();
        
        btnAjuda.setText("Ajuda");
        btnAjuda.setToolTipText("Clique aqui se estiver a ter dúvidas.");
        btnAjuda.setMnemonic(KeyEvent.VK_A);
        btnAjuda.setIcon(new ImageIcon("question2.png"));
        
        btnAjuda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(SelecionarAlgoritmoDialogo.this, "3 Mecanismos de Atribuição de Candidaturas Disponíveis\n\n"
                        + "Atribuição por Carga Equitativa:\n"
                        + "Atribui os FAE's às candidaturas, de modo que "
                        + "cada candidatura tenha mais ao menos o mesmo "
                        + "número de FAE's a avaliá-la.\n\n"
                        + "Atribuição por Número de FAE's pretendidos:\n"
                        + "Introduza o número de FAE's que deseja que cada "
                        + "candidatura tenha a avaliá-la.\n\n"
                        + "Atribuição por Experiência Profissional:\n"
                        + "Atribui mais candidaturas aos FAE's com mais experiência profissional, em proporção.", "AJUDA", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("info.png"));
            }
            
        });
        
        return btnAjuda;
    }
    
    /**
     * Cria e mostra ao utilizador uma caixa de diálogo com as atribuições geradas pelo mecanismo escolhido.
     * @param atr a lista de atribuições geradas
     * @param alg o algoritmo de atribuições selecionado
     */
    private void mostrarDialogoAtribuicoes(ArrayList<Atribuicao> atr, AlgoritmoAtribuicao alg) {
        JDialog mostrarDialogoAtribuicoes = new JDialog(this);

        JPanel painelTitulo = new JPanel();
        
        if (alg instanceof AlgoritmoAtribuicaoCargaEquitativa) {
            painelTitulo.add(new JLabel("Atribuições Geradas por Carga Equitativa"));
        } else if (alg instanceof AlgoritmoAtribuicaoNFAECandidatura) {
            painelTitulo.add(new JLabel("Atribuições Geradas com " + ((AlgoritmoAtribuicaoNFAECandidatura) alg).getNFAE() + " FAE's por Candidatura"));
        } else {
            painelTitulo.add(new JLabel("Atribuições Geradas com base na Experiência Profissional"));
        }
        
        JPanel painelAtribuicoes = new JPanel();
        JTextArea txtAtribuicoes = new JTextArea(atr.toString(), 14, 24);
        txtAtribuicoes.setEditable(false);
        
        // criar linha de contorno à volta da caixa de texto
        Border border = BorderFactory.createLineBorder(Color.GRAY);
        txtAtribuicoes.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        
        JScrollPane scrPane = new JScrollPane(txtAtribuicoes);
        
        // mostrar sempre a barra vertical de scroll
        scrPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        
        scrPane.setBorder(
            BorderFactory.createTitledBorder("Atribuições entre Candidaturas e FAE"));
        
        painelAtribuicoes.add(scrPane);
        
        mostrarDialogoAtribuicoes.add(painelTitulo, BorderLayout.NORTH);
        mostrarDialogoAtribuicoes.add(painelAtribuicoes, BorderLayout.CENTER);

        JPanel painelBotoes = new JPanel();
        
        JButton btnOK = new JButton("OK");
        btnOK.setMnemonic(KeyEvent.VK_O);
        
        btnOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mostrarDialogoAtribuicoes.dispose();
            }
            
        });
        
        painelBotoes.add(btnOK);
        
        JButton btnDefinitiva = new JButton("Guardar Definitivamente");
        btnDefinitiva.setMnemonic(KeyEvent.VK_G);
        
        btnDefinitiva.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ev.setAtribuicoes(atr);
                JOptionPane.showMessageDialog(SelecionarAlgoritmoDialogo.this, "Atribuições guardadas com sucesso!", "Sucesso na gravação dos dados", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("info.png"));
                
            }
            
        });
        
        painelBotoes.add(btnDefinitiva);
        
        mostrarDialogoAtribuicoes.add(painelBotoes, BorderLayout.SOUTH);
        
        mostrarDialogoAtribuicoes.setIconImage(new ImageIcon("info.png").getImage());
        mostrarDialogoAtribuicoes.pack();
        mostrarDialogoAtribuicoes.setMinimumSize(new Dimension(mostrarDialogoAtribuicoes.getWidth(), mostrarDialogoAtribuicoes.getHeight()));
        mostrarDialogoAtribuicoes.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        mostrarDialogoAtribuicoes.setLocationRelativeTo(this);
        mostrarDialogoAtribuicoes.setVisible(true);
    }

}
