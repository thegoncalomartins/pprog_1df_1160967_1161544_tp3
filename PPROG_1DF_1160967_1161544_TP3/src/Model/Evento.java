/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Utils.Data;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author m
 */
public class Evento implements Serializable{

    /**
     * O título do evento.
     */
    private String titulo;

    /**
     * Um texto descritivo sobre o âmbito do evento.
     */
    private String textoDesc;

    /**
     * A data de início do evento.
     */
    private Data dataInicio;

    /**
     * A data de fim do evento.
     */
    private Data dataFim;

    /**
     * O local do evento.
     */
    private String local;

    /**
     * A data de início da submissão de candidaturas ao evento.
     */
    private Data subInicio;

    /**
     * A data de fim da submissão de candidaturas ao evento.
     */
    private Data subFim;

    /**
     * ListaOrganizadores.
     */
    private ListaOrganizadores listaOrganizadores;

    /**
     * ListaFAE.
     */
    private ListaFAE listaFAE;

    /**
     * ListaCandidaturas.
     */
    private ListaCandidaturas listaCandidaturas;
    
    /**
     * A lista de atribuições
     */
    private ArrayList<Atribuicao> atribuicoes;

    /**
     * O título do evento por omissão.
     */
    private static final String TITULO_POR_OMISSAO = "Sem título";

    /**
     * O texto descritivo do evento por omissão.
     */
    private static final String TEXTO_DESC_POR_OMISSAO = "Sem texto descritivo";

    /**
     * A data de início do evento por omissão.
     */
    private static final Data DATA_INICIO_POR_OMISSAO = new Data();

    /**
     * A data de fim do evento por omissão.
     */
    private static final Data DATA_FIM_POR_OMISSAO = new Data();

    /**
     * O local do evento por omissão.
     */
    private static final String LOCAL_POR_OMISSAO = "Sem local";

    /**
     * A data de início da submissão de candidaturas ao evento por omissão.
     */
    private static final Data SUB_INICIO_POR_OMISSAO = new Data();

    /**
     * A data de fim da submissão de candidaturas ao evento por omissão.
     */
    private static final Data SUB_FIM_POR_OMISSAO = new Data();

    /**
     * Construtor completo.
     *
     * @param titulo o título do evento
     * @param textoDesc o texto descritivo do evento
     * @param dataInicio a data de início do evento
     * @param dataFim a data de fim do evento
     * @param local o local do evento
     * @param subInicio a data de início da submissão de candidaturas ao evento
     * @param subFim a data de fim da submissão de candidaturas ao evento
     */
    public Evento(String titulo, String textoDesc, Data dataInicio, Data dataFim, String local, Data subInicio, Data subFim) {

        this.titulo = titulo;
        this.textoDesc = textoDesc;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.local = local;
        this.subInicio = subInicio;
        this.subFim = subFim;
        listaOrganizadores = new ListaOrganizadores();
        listaFAE = new ListaFAE();
        listaCandidaturas = new ListaCandidaturas();
        atribuicoes = new ArrayList<>();
    }

    /**
     * Construtor vazio.
     */
    public Evento() {

        titulo = TITULO_POR_OMISSAO;
        textoDesc = TEXTO_DESC_POR_OMISSAO;
        dataInicio = DATA_INICIO_POR_OMISSAO;
        dataFim = DATA_FIM_POR_OMISSAO;
        local = LOCAL_POR_OMISSAO;
        subInicio = SUB_INICIO_POR_OMISSAO;
        subFim = SUB_FIM_POR_OMISSAO;
        listaOrganizadores = new ListaOrganizadores();
        listaFAE = new ListaFAE();
        listaCandidaturas = new ListaCandidaturas();
        atribuicoes = new ArrayList<>();
    }

    /**
     * Devolve o título do evento.
     *
     * @return o título do evento
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * Devolve o texto descritivo do evento.
     *
     * @return o texto descritivo do evento
     */
    public String getTextoDesc() {
        return textoDesc;
    }

    /**
     * Devolve a data de início do evento.
     *
     * @return a data de início do evento
     */
    public Data getDataInicio() {
        return dataInicio;
    }

    /**
     * Devolve a data de fim do evento.
     *
     * @return a data de fim do evento
     */
    public Data getDataFim() {
        return dataFim;
    }

    /**
     * Devolve o local do evento.
     *
     * @return o local do evento
     */
    public String getLocal() {
        return local;
    }

    /**
     * Devolve a data de início da submissão de candidaturas ao evento.
     *
     * @return a data de início da submissão de candidaturas ao evento
     */
    public Data getSubInicio() {
        return subInicio;
    }

    /**
     * Devolve a data de fim da submissão de candidaturas ao evento.
     *
     * @return a data de fim da submissão de candidaturas ao evento
     */
    public Data getSubFim() {
        return subFim;
    }

    /**
     * Devolve listaOrganizadores.
     *
     * @return listaOrganizadores
     */
    public ListaOrganizadores getListaOrganizadores() {
        return listaOrganizadores;
    }

    /**
     * Devolve listaFAE.
     *
     * @return listaFAE
     */
    public ListaFAE getListaFAE() {
        return listaFAE;
    }

    /**
     * Devolve listaCandidaturas.
     *
     * @return listaCandidaturas
     */
    public ListaCandidaturas getListaCandidaturas() {
        return listaCandidaturas;
    }
    
    /**
     * Devolve a lista de atribuições do evento.
     * @return a lista de atribuições do evento
     */
    public ArrayList<Atribuicao> getAtribuicoes() {
        return new ArrayList<>(atribuicoes);
    }

    /**
     * Altera o título do evento.
     *
     * @param titulo o novo título do evento
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * Altera o texto descritivo do evento.
     *
     * @param textoDesc o novo texto descritivo do evento
     */
    public void setTextoDesc(String textoDesc) {
        this.textoDesc = textoDesc;
    }

    /**
     * Altera a data de início do evento.
     *
     * @param dataInicio a nova data de início do evento
     */
    public void setDataInicio(Data dataInicio) {
        this.dataInicio = dataInicio;
    }

    /**
     * Altera a data de fim do evento.
     *
     * @param dataFim a nova data de fim do evento
     */
    public void setDataFim(Data dataFim) {
        this.dataFim = dataFim;
    }

    /**
     * Altera o local do evento
     *
     * @param local o novo local do evento
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * Altera a data de início da submissão de candidaturas ao evento.
     *
     * @param subInicio a nova data de início
     */
    public void setSubInicio(Data subInicio) {
        this.subInicio = subInicio;
    }

    /**
     * Altera a data de início da submissão de candidaturas ao evento.
     *
     * @param subFim a nova data de fim
     */
    public void setSubFim(Data subFim) {
        this.subFim = subFim;
    }
    
    /**
     * Define a lista de atribuições do evento como sendo a passada por parâmetro.
     * @param atr a lista de atribuições
     */
    public void setAtribuicoes(ArrayList<Atribuicao> atr) {
        this.atribuicoes = new ArrayList<>(atr);
    }

    /**
     * Devolve a descrição geral do evento.
     *
     * @return caraterísticas do evento
     */
    @Override
    public String toString() {

        return String.format("TÍTULO: %s"
                + "%nTEXTO DESCRITIVO: %s"
                + "%nDATA INÍCIO: %s"
                + "%nDATA FIM: %s"
                + "%nLOCAL: %s"
                + "%nDATA INÍCIO SUBMISSÂO: %s"
                + "%nDATA FIM SUBMISSÂO: %s", titulo, textoDesc, dataInicio, dataFim, local, subInicio, subFim);
    }

    /**
     * Compara o evento com o objeto recebido.
     *
     * @param obj o objeto a comparar com o evento
     * @return true se o objeto recebido representar um evento equivalente ao
     *         evento. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Evento e = (Evento) obj;

        return this.titulo.equalsIgnoreCase(e.titulo) && this.textoDesc.equalsIgnoreCase(e.textoDesc)
                && this.dataInicio.equals(e.dataInicio) && this.dataFim.equals(e.dataFim) && this.local.equalsIgnoreCase(e.local)
                && this.subInicio.equals(e.subInicio) && this.subFim.equals(e.subFim) && this.listaOrganizadores.equals(e.listaOrganizadores)
                && this.listaFAE.equals(e.listaFAE) && this.listaCandidaturas.equals(e.listaCandidaturas) && this.atribuicoes.equals(e.atribuicoes);
    }

}
