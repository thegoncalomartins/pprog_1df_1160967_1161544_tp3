/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 * Interface que contém o método "atribui", para poder ser reescrito nas classes de Algoritmos que a implementam.
 * 
 * @author Gonçalo Martins
 */
public interface AlgoritmoAtribuicao {
    
    /**
     * Método que recebe um evento por parâmetro e faz as atribuições entre as candidaturas desse evento e os respetivos FAE's.
     * @param e o evento que contém as candidaturas e os FAE's a serem atribuídos
     * @return a lista de atribuições geradas
     */
    public ArrayList<Atribuicao> atribui(Evento e);
}
