/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 * A classe Produto. Esta classe foi criada com o intuito de os produtos não serem considerados meras "Strings" com os seus nomes.
 * E também com o propósito de pensar um pouco mais além, como Engenheiros Informáticos que seremos no futuro.
 * @author Gonçalo Martins
 */
public class Produto implements Serializable{
    
    // um produto tem nome, mas terá ou não quantidade??
    // existem mais atributos que devemos considerar?
    
    /**
     * O nome do Produto.
     */
    private String nomeProduto;
    
    /**
     * A quantidade do Produto.
     */
    private int quantidade;
    
    /**
     * O nome por omissão do produto.
     */
    private static final String NOME_PRODUTO_OMISSAO = "Produto sem nome";
    
    /**
     * A quantidade por omissão do produto.
     */
    private static final int QUANTIDADE_OMISSAO = 0;
    
    /**
     * Constrói uma nova instância de produto com todos os parâmetros.
     * @param nome o nome do produto
     * @param quantidade a quantidade do produto
     */
    public Produto(String nome, int quantidade) {
        nomeProduto = nome;
        this.quantidade = quantidade;
    }
    
    /**
     * Constrói uma instância de produto com os atributos por omissão.
     */
    public Produto() {
        nomeProduto = NOME_PRODUTO_OMISSAO;
        quantidade = QUANTIDADE_OMISSAO;
    }
    
    /**
     * Constrói uma cópia de um produto.
     * 
     * @param produto o produto a ser copiado
     */
    public Produto(Produto produto) {
        this.nomeProduto = produto.nomeProduto;
        this.quantidade = produto.quantidade;
    }

    /**
     * Devolve o nome do produto.
     * 
     * @return o nome do produto
     */
    public String getNomeProduto() {
        return nomeProduto;
    }

    /**
     * Devolve a quantidade de produto.
     * 
     * @return a quantidade de produto
     */
    public int getQuantidade() {
        return quantidade;
    }

    /**
     * Altera o nome do produto.
     * 
     * @param nomeProduto o nome do produto a definir
     */
    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    /**
     * Altera a quantidade de produto.
     * 
     * @param quantidade quantidade de produto a definir
     */
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    /**
     * Devolve a descrição geral do produto.
     * 
     * @return caraterísticas do produto
     */
    @Override
    public String toString() {
        return String.format("PRODUTO: %s%nQUANTIDADE: %d", nomeProduto, quantidade);
    }
    
    /**
     * Compara o produto com o objeto recebido.
     *
     * @param obj o objeto a comparar com o produto
     * @return true se o objeto recebido representar um produto equivalente ao
     *         produto. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }
        
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        
        Produto p = (Produto) obj;
        
        return p.nomeProduto.equalsIgnoreCase(this.nomeProduto) && p.quantidade == this.quantidade;
    }
}
