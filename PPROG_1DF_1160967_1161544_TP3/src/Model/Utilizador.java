/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 * A classe utilizador.
 * @author m
 */
public class Utilizador implements Serializable {
    
    /**
     * O nome do utilizador.
     */
    private String nome;
    
    /**
     * O email do utilizador.
     */
    private String email;
    
    /**
     * O username do utilizador.
     */
    private String username;
    
    /**
     * A password do utilizador.
     */
    private String password;
    
    /**
     * O nome do utilizador por omissão.
     */
    private static final String NOME_POR_OMISSAO = "Sem nome";
    
    /**
     * O email do utilizador por omissão.
     */
    private static final String EMAIL_POR_OMISSAO = "Sem email";
    
    /**
     * O username do utilizador por omissão.
     */
    private static final String USERNAME_POR_OMISSAO = "Sem username";
    
    /**
     * A password do utilizador por omissão.
     */
    private static final String PASSWORD_POR_OMISSAO = "Sem password";
    
    /**
     * Construtor completo.
     * 
     * @param nome o nome do utilizador 
     * @param email o email do utilizador
     * @param username o username do utilizador
     * @param password a password do utilizador
     */
    public Utilizador(String nome, String email, String username, String password) {
        
        this.nome = nome;
        this.email = email;
        this.username = username;
        this.password = password;
    }
    
    /**
     * Construtor vazio.
     */
    public Utilizador() {
        
        nome = NOME_POR_OMISSAO;
        email = EMAIL_POR_OMISSAO;
        username = USERNAME_POR_OMISSAO;
        password = PASSWORD_POR_OMISSAO;
    }

    /**
     * Devolve o nome do utilizador. 
     * 
     * @return o nome do utilizador
     */
    public String getNome() {
        return nome;
    }

    /**
     * Devolve o email do utilizador.
     * 
     * @return o email do utilizador
     */
    public String getEmail() {
        return email;
    }

    /**
     * Devolve o username do utilizador.
     * 
     * @return o username do utilizador
     */
    public String getUsername() {
        return username;
    }

    /**
     * Devolve a password do utilizador.
     * 
     * @return a password do utilizador
     */
    public String getPassword() {
        return password;
    }

    /**
     * Altera o nome do utilizador.
     * 
     * @param nome o novo nome do utilizador
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Altera o email do utilizador.
     * 
     * @param email o novo email do utilizador
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Altera o username do utilizador.
     * 
     * @param username o novo username do utilizador
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Altera a password do utilizador.
     * 
     * @param password a nova password do utilizador
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * Devolve a descrição geral do utilizador.
     * 
     * @return caraterísticas do utilizador
     */
    @Override
    public String toString() {
        
        return String.format("NOME: %s"
                + "%nEMAIL: %s"
                + "%nUSERNAME: %s", nome, email, username);
    }
    
    /**
     * Compara o utilizador com o objeto recebido.
     *
     * @param obj o objeto a comparar com o utilizador
     * @return true se o objeto recebido representar um utilizador equivalente ao
     *         utilizador. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        
        Utilizador u = (Utilizador) obj;
        
        return this.nome.equalsIgnoreCase(u.nome) && this.email.equalsIgnoreCase(u.email) 
                && this.username.equalsIgnoreCase(u.username) && this.password.equalsIgnoreCase(u.password);
    }
}
