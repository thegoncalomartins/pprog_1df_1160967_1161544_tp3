/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A classe responsável por conter a lista de Organizadores de um evento.
 * 
 * @author m
 */
public class ListaOrganizadores implements Serializable{

    /**
     * A lista de organizadores.
     */
    private final ArrayList<Organizador> organizadores;

    /**
     * Construtor que instancia a lista de organizadores.
     */
    public ListaOrganizadores() {

        organizadores = new ArrayList<>();
    }

    /**
     * Devolve a lista de organizadores.
     *
     * @return a lista de organizadores
     */
    public ArrayList<Organizador> getOrganizadores() {

        return organizadores;
    }

    /**
     * Adiciona um organizador recebido por parâmetro à lista de organizadores.
     *
     * @param org o organizador a ser adicionado
     * @return true se o organizador não existir na lista e for adicionado, false caso contrário
     */
    public boolean addOrganizador(Organizador org) {

        if (!organizadores.contains(org)) {
            return organizadores.add(org);
        }
        return false;
    }
}
