/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Utils.Data;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

/**
 * A classe que possui atributos e métodos para a escrita e leitura de ficheiros de texto/binários.
 * 
 * @author Gonçalo Martins
 */
public class FicheiroCentroEventos {
    
    /**
     * Variável de classe que contém o nome do ficheiro binário a utilizar.
     */
    public static final String NOME_FICHEIRO_BINARIO = "centroEventos.bin";
    
    /**
     * Variável de classe que contém o nome do ficheiro de texto a utilizar.
     */
    public static final String NOME_FICHEIRO_TEXTO = "centroEventos.txt";
    
    /**
     * Construtor.
     */
    public FicheiroCentroEventos() {
    }
    
    /**
     * Método que lê um ficheiro de texto e instancia os objetos do centro de eventos.
     * 
     * @param nomeFich o nome do ficheiro de texto a ser lido
     * @param ce o objeto centro de eventos
     * @throws FileNotFoundException exceção lançada quando o ficheiro não é encontrado
     */
    public void lerFicheiroTexto(String nomeFich, CentroDeEventos ce) throws FileNotFoundException {
        String linha;
        String[] dados;
        Scanner ler = new Scanner(new File(nomeFich));
        while (ler.hasNext()) {
            linha = ler.nextLine();
            if (linha.length() > 0) {
                dados = linha.split(";");
                selecionarClasse(dados, ce);
            }
        }
    } 
    
    /**
     * Método que seleciona a classe a ser instanciada.
     * @param dados a string que contém os atributos do objeto a ser instanciado
     * @param ce o objeto centro de eventos.
     */
    private void selecionarClasse(String[] dados, CentroDeEventos ce) {

        String tituloEvento;
        String user;
        switch (dados[0].trim().toLowerCase()) {
            case "utilizador":
                String nome = dados[1].trim();
                String email = dados[2].trim();
                String username = dados[3].trim();
                String password = dados[4].trim();
                criarEAdicionarUtilizador(nome, email, username, password, ce);
                break;
            case "evento":
                String titulo = dados[1].trim();
                String textoDesc = dados[2].trim();
                String dataInicio = dados[3].trim();
                String dataFim = dados[4].trim();
                String local = dados[5].trim();
                String subInicio = dados[6].trim();
                String subFim = dados[7].trim();
                criarEAdicionarEvento(titulo, textoDesc, dataInicio, dataFim, local, subInicio, subFim, ce);
                break;
            case "organizador":
                tituloEvento = dados[1].trim();
                user = dados[2].trim();
                criarEAdicionarOrganizador(tituloEvento, user, ce);
                break;
            case "fae":
                tituloEvento = dados[1].trim();
                user = dados[2].trim();
                int candidaturasAvaliadas = Integer.parseInt(dados[3].trim());
                criarEAdicionarFAE(tituloEvento, user, candidaturasAvaliadas, ce);
                break;
            case "candidatura":
                tituloEvento = dados[1].trim();
                String empresa = dados[2].trim();
                String morada = dados[3].trim();
                String telemovel = dados[4].trim();
                float areaExposicao = Float.parseFloat(dados[5].trim());
                String produtos = dados[6].trim();
                int numeroConvidados = Integer.parseInt(dados[7].trim());
                criarEAdicionarCandidatura(tituloEvento, empresa, morada, telemovel, areaExposicao, produtos, numeroConvidados, ce);
                break;

        }
    }
    
    /**
     * Cria e adiciona um utilizador ao registo de utilizadores do centro de eventos.
     * @param nome o nome do utilizador
     * @param email o seu e-mail
     * @param username o seu usarname
     * @param password a sua password
     * @param ce o objeto centro de eventos
     */
    private void criarEAdicionarUtilizador(String nome, String email, String username, String password, CentroDeEventos ce) {

        Utilizador utilizador = new Utilizador(nome, email, username, password);
        ce.getRegistoUtilizadores().addUtilizador(utilizador);
    }
    
    /**
     * Cria e adiciona um evento com os dados passados por parâmetro.
     *
     * @param titulo o título do evento
     * @param textoDesc o texto descritivo do evento
     * @param dInicio a data de início do evento no formato ano/mês/dia
     * @param dFim a data de fim do evento no formato ano/mês/dia
     * @param local o local do evento
     * @param sInicio a data de início da submissão de candidaturas ao evento no formato ano/mês/dia
     * @param sFim a data de fim da submissão de candidaturas ao evento no formato ano/mês/dia
     * @param ce o centro de eventos
     */
    private void criarEAdicionarEvento(String titulo, String textoDesc, String dInicio, String dFim, String local,
            String sInicio, String sFim, CentroDeEventos ce) {

        String[] dInicioAuxString = dInicio.split("/");
        String[] dFimAuxString = dFim.split("/");
        String[] sInicioAuxString = sInicio.split("/");
        String[] sFimAuxString = sFim.split("/");

        int[] dInicioAuxInt = new int[dInicioAuxString.length];
        int[] dFimAuxInt = new int[dFimAuxString.length];
        int[] sInicioAuxInt = new int[sInicioAuxString.length];
        int[] sFimAuxInt = new int[sFimAuxString.length];

        for (int i = 0; i < dInicioAuxString.length; i++) {
            dInicioAuxInt[i] = Integer.parseInt(dInicioAuxString[i].trim());
            dFimAuxInt[i] = Integer.parseInt(dFimAuxString[i].trim());
            sInicioAuxInt[i] = Integer.parseInt(sInicioAuxString[i].trim());
            sFimAuxInt[i] = Integer.parseInt(sFimAuxString[i].trim());
        }

        Data dataInicio = new Data(dInicioAuxInt[0], dInicioAuxInt[1], dInicioAuxInt[2]);
        Data dataFim = new Data(dFimAuxInt[0], dFimAuxInt[1], dFimAuxInt[2]);
        Data subInicio = new Data(sInicioAuxInt[0], sInicioAuxInt[1], sInicioAuxInt[2]);
        Data subFim = new Data(sFimAuxInt[0], sFimAuxInt[1], sFimAuxInt[2]);

        Evento evento = new Evento(titulo, textoDesc, dataInicio, dataFim, local, subInicio, subFim);
        ce.getRegistoEventos().adicionarEvento(evento);

    }
    
    /**
     * Cria um organizador com o utilizador correspondente ao username passado por parâmetro, e adiciona-o ao evento passado por parâmetro.
     *
     * @param tituloEvento o título do evento
     * @param username o username do utilizador
     * @param ce o centro de eventos
     */
    private void criarEAdicionarOrganizador(String tituloEvento, String username, CentroDeEventos ce) {

        Evento evento = ce.getRegistoEventos().procurarEventoPorTitulo(tituloEvento);
        Utilizador utilizador = ce.getRegistoUtilizadores().procurarUtilizadorPorUsername(username);

        Organizador org = new Organizador(utilizador);
        if (evento != null && utilizador != null) {
            evento.getListaOrganizadores().addOrganizador(org);
        }
    }
    
    /**
     * Cria um FAE com o utilizador correspondente ao username passado por parâmetro, e adiciona-o ao evento passado por parâmetro.
     *
     * @param tituloEvento o título do evento
     * @param username o username do utilizador
     * @param ce o centro de eventos
     */
    private void criarEAdicionarFAE(String tituloEvento, String username, int candidaturasAvaliadas, CentroDeEventos ce) {

        Evento evento = ce.getRegistoEventos().procurarEventoPorTitulo(tituloEvento);
        Utilizador utilizador = ce.getRegistoUtilizadores().procurarUtilizadorPorUsername(username);

        FAE f = new FAE(utilizador, candidaturasAvaliadas);
        if (evento != null && utilizador != null) {
            evento.getListaFAE().addFAE(f);
        }
    }
    
    /**
     * Cria uma candidatura com os dados passados por parâmetro, e adiciona-a ao evento passado por parâmetro.
     *
     * @param tituloEvento o título do evento
     * @param empresa a empresa
     * @param morada a morada
     * @param telemovel o nº de telemóvel
     * @param areaExposicao a área de exposição
     * @param produtosAExpor os produtos a expor
     * @param numeroConvidados o nº de convidados
     * @param ce o centro de eventos
     */
    private void criarEAdicionarCandidatura(String tituloEvento, String empresa, String morada, String telemovel, float areaExposicao, String produtosAExpor, int numeroConvidados, CentroDeEventos ce) {

        Evento evento = ce.getRegistoEventos().procurarEventoPorTitulo(tituloEvento);

        Candidatura c = evento.getListaCandidaturas().novaCandidatura(empresa, morada, telemovel, areaExposicao, numeroConvidados);

        String[] produtos = produtosAExpor.split("-");
        for (int i = 0; i < produtos.length; i += 2) {
            String nomeProduto = produtos[i];
            int quantidade = Integer.parseInt(produtos[i + 1]);
            Produto p = new Produto(nomeProduto, quantidade);
            c.adicionarProdutoAExpor(p);
        }
        if (evento != null) {
            evento.getListaCandidaturas().registarCandidatura(c);
        }
    }
    
    /**
     * Método que lê um ficheiro binário com um nome passado por parâmetro e retorna um objeto do tipo CentroDeEventos.
     * @param nomeFich o nome do ficheiro binário a ser lido
     * @return o objeto lido do ficheiro binário e null se não conseguiu ler o ficheiro ou a classe não foi encontrada.
     */
    public CentroDeEventos ler(String nomeFich) {
        CentroDeEventos ce;
        try {
            ObjectInputStream in = new ObjectInputStream(
                    new FileInputStream(nomeFich));
            try {
                ce = (CentroDeEventos) in.readObject();
                
            } finally {
                in.close();
            }
            return ce;
        } catch (IOException | ClassNotFoundException ex) {
            return null;
        }
    }
    
    /**
     * Guarda num ficheiro binário um objeto do tipo CentroDeEventos. Retorna true se conseguiu, false caso contrário.
     * @param nomeFich o nome do ficheiro binário
     * @param ce o objeto centro de eventos que pretendemos guardar
     * @return true se a gravação no ficheiro ocorreu com sucesso, false caso contrário
     */
    public boolean guardar(String nomeFich, CentroDeEventos ce) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFich));
            try {
                out.writeObject(ce);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
    
    
}
