/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 * A classe Decisão.
 * @author Gonçalo Martins
 */
public class Decisao implements Serializable{
    
    /**
     * Booleano que indica a decisão, aceite (true) ou rejeitada (false) .
     */
    private boolean decisao;
    
    /**
     * A justificação da decisão.
     */
    private String textoJustificativo;
    
    /**
     * Booleano que indica a decisão por omissão (rejeitada - false).
     */
    private static final boolean DECISAO_POR_OMISSAO = false;
    
    /**
     * O texto justificativo por omissão.
     */
    private static final String TEXTO_JUSTIFICATIVO_OMISSAO = "Sem justificação";
    
    /**
     * Constrói uma nova decisão com todos os atributos necessários.
     * 
     * @param decisao booleano que indica se a candidatura foi aceite ou rejeitada (true para aceite, false para rejeitada)
     * @param textoJustificativo o texto justificativo da decisão tomada
     */
    public Decisao(boolean decisao, String textoJustificativo) {
        this.decisao = decisao;
        setTextoJustificativo(textoJustificativo);
    }
    
    /**
     * Constrói uma nova decisão com os atributos por omissão.
     */
    public Decisao() {
        decisao = DECISAO_POR_OMISSAO;
        textoJustificativo = TEXTO_JUSTIFICATIVO_OMISSAO;
    }
    
    /**
     * Constrói uma cópia de uma decisão.
     * 
     * @param decisao a decisão a ser copiada
     */
    public Decisao(Decisao decisao) {
        this.decisao = decisao.decisao;
        this.textoJustificativo = decisao.textoJustificativo;
    }

    /**
     * Devolve a decisão.
     * 
     * @return a decisão
     */
    public boolean getDecisao() {
        return decisao;
    }

    /**
     * Devolve o texto justificativo da decisão.
     * 
     * @return o texto justificativo da decisão
     */
    public String getTextoJustificativo() {
        return textoJustificativo;
    }

    /**
     * Altera o booleano decisão.
     * 
     * @param decisao a decisao a definir
     */
    public void setDecisao(boolean decisao) {
        this.decisao = decisao;
    }

    /**
     * Altera o texto justificativo.
     * @param textoJustificativo o texto justificativo a definir
     */
    public void setTextoJustificativo(String textoJustificativo) {
        
        if (textoJustificativo.trim().isEmpty()) {
            throw new IllegalArgumentException("O texto justificativo inserido é inválido!");
        }
        
        this.textoJustificativo = textoJustificativo;
    }
    
    /**
     * Devolve a descrição geral da decisão.
     *
     * @return caraterísticas da decisão
     */
    @Override
    public String toString() {
        if (decisao) {
            return String.format("ACEITE: %s%nTEXTO JUSTIFICATIVO: %s%n", "SIM", textoJustificativo);
        }
        return String.format("ACEITE: %s%nTEXTO JUSTIFICATIVO: %s%n", "NÃO", textoJustificativo);
    }
    
    /**
     * Compara a decisão com o objeto recebido.
     *
     * @param obj o objeto a comparar com a decisão
     * @return true se o objeto recebido representar uma decisão equivalente à
     *         decisão. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        
        Decisao c = (Decisao) obj;
        
        return this.decisao == c.decisao && this.textoJustificativo.equalsIgnoreCase(c.textoJustificativo);
    }
    
    
}
