/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 * A Classe Centro de Eventos
 * @author m
 */
public class CentroDeEventos implements Serializable {
    
    /**
     * RegistoEventos.
     */
    private RegistoEventos registoEventos;
    
    /**
     * RegistoUtilizadores.
     */
    private RegistoUtilizadores registoUtilizadores;
    
    /**
     * RegistoAlgoritmos.
     */
    private RegistoAlgoritmos registoAlgoritmos;
    
    /**
     * Construtor que instância RegistoEventos e RegistoUtilizadores.
     */
    public CentroDeEventos() {
        
        registoEventos = new RegistoEventos();
        registoUtilizadores = new RegistoUtilizadores();
        registoAlgoritmos = new RegistoAlgoritmos();
    }
    
    /**
     * Devolve registoEventos.
     * 
     * @return registoEventos 
     */
    public RegistoEventos getRegistoEventos() {
        return registoEventos;
    }
    
    /**
     * Devolve registoUtilizadores.
     * 
     * @return registoUtilizadores
     */
    public RegistoUtilizadores getRegistoUtilizadores() {
        return registoUtilizadores;
    }    
    
    /**
     * Devolve o registo de algoritmos de atribuição disponíveis.
     * @return o registo de algoritmos de atribuição disponíveis no centro de eventos 
     */
    public RegistoAlgoritmos getRegistoAlgoritmos() {
        return registoAlgoritmos;
    }
}
