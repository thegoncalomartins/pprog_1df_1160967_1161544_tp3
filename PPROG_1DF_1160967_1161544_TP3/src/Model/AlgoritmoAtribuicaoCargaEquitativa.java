/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Algoritmo de atribuição por carga equitativa.
 * @author m
 */
public class AlgoritmoAtribuicaoCargaEquitativa implements AlgoritmoAtribuicao, Serializable {

    /**
     * Construtor do algoritmo.
     */
    public AlgoritmoAtribuicaoCargaEquitativa() {

    }

    /**
     * Método que atribui as candidaturas por decidir aos FAE's (por carga equitativa)
     *
     * @param evento o evento que possui as candidaturas a serem distribuídas pelos FAE
     * @return a lista de atribuições realizadas
     */
    @Override
    public ArrayList<Atribuicao> atribui(Evento evento) {
        ArrayList<Atribuicao> atribuicoes = new ArrayList();
        ArrayList<Candidatura> lc = evento.getListaCandidaturas().getCandidaturas();
        ArrayList<FAE> lf = evento.getListaFAE().getFae();
        int c = lc.size();
        int f = lf.size();
        int q = c / f;
        int r = c % f;

        // cria cópia da lista de candidaturas
        ArrayList<Candidatura> lc2 = new ArrayList<>(lc);

        // baralha a lista, para permitir uma atribuição aleatória aos FAE's
        Collections.shuffle(lc2);

//        se o nº de candidaturas for maior ou igual ao nº de fae, distribui normalmente por ordem ate acabarem as candidaturas
        if (q <= 1) {

            // se só houver uma candidatura, distribui a candidatura por todos os FAE's
            if (c == 1) {
                for (int i = 0; i < f; i++) {
                    atribuicoes.add(new Atribuicao(lc.get(0), lf.get(i)));
                }
            } else {
                for (int i = 0; i < c; i++) {
                    atribuicoes.add(new Atribuicao(lc2.get(i), lf.get(i)));
                }
            }

        } else {
//            se o nº de candidaturas for maior que o nº de fae, distribui candidaturas pelos fae de forma a que cada fae fique com igual nº de candidaturas
//            ex: 12 candidaturas 5 fae, distribui 2 candidaturas para cada fae      
            for (int i = 0; i < q; i++) {
                for (int j = 0; j < f; j++) {
                    atribuicoes.add(new Atribuicao(lc2.get(j + i * f), lf.get(j)));
                }
            }
//            distribui as candidaturas que restam
            for (int i = c - r; i < c; i++) {
                atribuicoes.add(new Atribuicao(lc2.get(i), lf.get(i - (c - r))));
            }
        }
        return atribuicoes;
    }

    /**
     * Devolve a descrição geral do algoritmo.
     * @return a descrição do algoritmo, neste caso o nome
     */
    @Override
    public String toString() {
        return String.format("%s", "Distribuição por Carga Equitativa");
    }
}
