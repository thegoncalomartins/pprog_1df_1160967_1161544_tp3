/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 * Classe Atribuicao.
 * @author m
 */
public class Atribuicao implements Serializable{
    
    /**
     * A candidatura.
     */
    private Candidatura candidatura;
    
    /**
     * O fae.
     */
    private FAE fae;
    
    /**
     * Construtor completo.
     * 
     * @param candidatura a candidatura
     * @param fae o fae
     */
    public Atribuicao(Candidatura candidatura, FAE fae) {
        
        this.candidatura = candidatura;
        this.fae = fae;
    }
    
    /**
     * Devolve a candidatura.
     * 
     * @return a candidatura
     */
    public Candidatura getCandidatura() {
        return candidatura;
    }
    
    /**
     * Devolve o fae.
     * 
     * @return o fae
     */
    public FAE getFae() {
        return fae;
    }
    
    
    /**
     * Altera a candidatura.
     * 
     * @param candidatura a nova candidatura
     */
    public void setCandidatura(Candidatura candidatura) {
        this.candidatura = candidatura;
    }
    
    /**
     * Altera o fae.
     * 
     * @param fae o novo fae 
     */
    public void setFae(FAE fae) {
        this.fae = fae;
    }
    
    /**
     * Devolve a descrição geral da atribuição.
     * 
     * @return caraterísticas da atribuição
     */
    @Override
    public String toString() {
        return String.format("%n%n###CANDIDATURA### %n%s"
                + "%n%s", candidatura, fae);
    }
    
    /**
     * Compara a atribuição com o objeto recebido.
     *
     * @param obj o objeto a comparar com a atribuição
     * @return true se o objeto recebido representar uma atribuição equivalente à
     *         atribuição. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        
        Atribuicao a = (Atribuicao) obj;
        return this.candidatura.equals(a.candidatura) && this.fae.equals(a.fae);
    }
}
