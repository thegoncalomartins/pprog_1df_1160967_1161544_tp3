/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Utils.Data;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A classe responsável por conter o registo de eventos do centro de eventos.
 * 
 * @author m
 */
public class RegistoEventos implements Serializable{

    /**
     * A lista de eventos.
     */
    private final ArrayList<Evento> eventos;

    /**
     * Construtor que instancia a lista de eventos.
     */
    public RegistoEventos() {

        eventos = new ArrayList<>();
    }
    
    /**
     * Devolve o evento correspondente ao índice passado por parâmetro.
     * 
     * @param indice o índice
     * @return o evento
     */
    public Evento obterEvento(int indice) {
        return eventos.get(indice);
    }
    
    /**
     * Devolve o tamanho da lista de eventos.
     * 
     * @return o tamanho da lista de eventos 
     */
    public int tamanho() {
        return eventos.size();
    }
    
    /**
     * Adiciona um evento recebido por parâmetro à lista de eventos.
     * 
     * @param evento o evento a ser adicionado
     * @return true se o evento não existir na lista e for adicionado, false caso contrário
     */
    public boolean adicionarEvento(Evento evento) {
        
        if (!eventos.contains(evento)) {
            return eventos.add(evento);
        }
        return false;
    }
    
    /**
     * Devolve um evento com o título passado por parâmetro. Se não existir nenhum devolve null.
     * 
     * @param titulo o titulo do evento
     * @return um evento com o título passado por parâmetro, null se não existir nenhum
     */
    public Evento procurarEventoPorTitulo(String titulo) {
        
        for (Evento evento : eventos) {
            if (titulo.equalsIgnoreCase(evento.getTitulo())) {
                return evento;
            }
        }
        return null;
    }

}
