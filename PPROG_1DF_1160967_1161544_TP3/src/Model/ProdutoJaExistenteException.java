/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 * A classe de produto já existente, uma classe de exceção.
 * @author Gonçalo Martins
 */
public class ProdutoJaExistenteException extends IllegalArgumentException implements Serializable {
    
    public ProdutoJaExistenteException(String msg) {
        super(msg);
    }
    
    public ProdutoJaExistenteException(Produto p) {
        super("O produto " + p.getNomeProduto() + " já se encontra na lista de produtos a expor da candidatura!");
    }
}
