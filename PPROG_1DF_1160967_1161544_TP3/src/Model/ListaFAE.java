/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A classe responsável por conter a lista de FAE's selecionados para um evento.
 * 
 * @author m
 */
public class ListaFAE implements Serializable{

    /**
     * A lista de fae.
     */
    private final ArrayList<FAE> fae;

    /**
     * Construtor que instancia a lista de fae.
     */
    public ListaFAE() {

        fae = new ArrayList<>();
    }

    /**
     * Devolve a lista de fae.
     *
     * @return a lista de fae
     */
    public ArrayList<FAE> getFae() {

        return fae;
    }

    /**
     * Adiciona um fae recebido por parâmetro à lista de fae.
     *
     * @param f o fae a ser adicionado
     * @return true se o fae não existir na lista e for adicionado, false caso contrário
     */
    public boolean addFAE(FAE f) {

        if (!fae.contains(f)) {
            return fae.add(f);
        }
        return false;
    }
}
