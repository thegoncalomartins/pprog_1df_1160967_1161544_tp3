/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Algoritmo de atribuição por número de FAE's pretendidos por candidatura.
 * 
 * @author m
 */
public class AlgoritmoAtribuicaoNFAECandidatura implements AlgoritmoAtribuicao, Serializable {
    
    
    /**
     * O número de fae por candidatura.
     */
    private int nFAE;
    
    /**
     * O número de fae por candidatura por omissão.
     */
    private static final int N_FAE_POR_OMISSAO = 1;
    
    /**
     * Construtor.
     */
    public AlgoritmoAtribuicaoNFAECandidatura() {
        
        nFAE = N_FAE_POR_OMISSAO;
    }
    
    /**
     * Devolve o número de FAE's especificado para avaliar cada candidatura.
     * @return o número de FAE's
     */
    public int getNFAE() {
        return nFAE;
    }
    
    /**
     * Altera o nº de fae por candidatura.
     * 
     * @param nFAE o novo nº de fae por candidatura
     */
    public void setNFAE(int nFAE) {
        
        this.nFAE = nFAE;
    }
    
    /**
     * Método que atribui as candidaturas por decidir aos FAE's, neste caso com um determinado número de FAE's por candidatura
     *
     * @param evento o evento que possui as candidaturas a serem distribuídas pelos FAE
     * @return a lista de atribuições realizadas
     */
    @Override
    public ArrayList<Atribuicao> atribui(Evento evento) throws NumberFormatException {
        ArrayList<Atribuicao> atribuicoes = new ArrayList();
        ArrayList<Candidatura> lc = evento.getListaCandidaturas().getCandidaturas();
        ArrayList<FAE> lf = evento.getListaFAE().getFae();

        int c = lc.size();
        int f = lf.size();

        if (nFAE > f) {
            throw new IllegalArgumentException("O número de FAE's pretendidos é superior ao número de FAE's disponíveis!");
        }

        // cria uma cópia da lista de FAE's do evento
        ArrayList<FAE> lf2 = new ArrayList<>(lf);

        for (int i = 0; i < c; i++) {
            // baralha a lista copiada
            Collections.shuffle(lf2);
            for (int j = 0; j < nFAE; j++) {
                // e faz a atribuição entre a candidatura e um fae escolhido da lista baralhada...
                atribuicoes.add(new Atribuicao(lc.get(i), lf2.get(j)));
            }
        }

        return atribuicoes;
    }
    
    /**
     * Devolve a descrição geral do objeto.
     * @return descrição do algoritmo, o seu nome 
     */
    @Override
    public String toString() {
        return String.format("%s", "Número de FAE's pretendidos por candidatura");
    }
}
