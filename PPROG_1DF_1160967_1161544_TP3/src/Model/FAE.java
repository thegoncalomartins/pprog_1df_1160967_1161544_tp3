/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 * A classe FAE (Funcionário de Apoio ao Evento)
 * 
 * @author m
 */
public class FAE implements Comparable<FAE>, Serializable {

    /**
     * O utilizador.
     */
    private Utilizador utilizador;

    /**
     * O nº de candidaturas avaliadas pelo fae.
     */
    private int candidaturasAvaliadas;

    /**
     * O utilizador por omissão.
     */
    private static final Utilizador UTILIZADOR_POR_OMISSAO = new Utilizador();

    /**
     * O nº de candidaturas avaliadas por omissão.
     */
    private static final int CANDIDATURAS_AVALIADAS_POR_OMISSAO = 0;

    /**
     * Construtor que recebe o utilizador como parâmetro.
     *
     * @param utilizador o utilizador
     * @param candidaturasAvaliadas o nº de candidaturas avaliadas pelo fae
     */
    public FAE(Utilizador utilizador, int candidaturasAvaliadas) {

        this.utilizador = utilizador;
        this.candidaturasAvaliadas = candidaturasAvaliadas;
    }

    /**
     * Construtor vazio.
     */
    public FAE() {

        utilizador = UTILIZADOR_POR_OMISSAO;
        candidaturasAvaliadas = CANDIDATURAS_AVALIADAS_POR_OMISSAO;
    }

    /**
     * Devolve o utilizador.
     *
     * @return o utilizador
     */
    public Utilizador getUtilizador() {

        return utilizador;
    }
    
    /**
     * Devolve o nº de candidaturas avaliadas pelo fae.
     * 
     * @return o nº de candidaturas avaliadas pelo fae
     */
    public int getCandidaturasAvaliadas() {
        
        return candidaturasAvaliadas;
    }
    
    /**
     * Altera o utilizador.
     * 
     * @param utilizador o novo utilizador
     */
    public void setUtilizador(Utilizador utilizador) {
        
        this.utilizador = utilizador;
    }
    
    /**
     * Altera o nº de candidaturas avaliadas pelo fae.
     * 
     * @param candidaturasAvaliadas o novo nº de candidaturas avaliadas pelo fae
     */
    public void setCandidaturasAvaliadas(int candidaturasAvaliadas) {
       
        this.candidaturasAvaliadas = candidaturasAvaliadas;
    }

    /**
     * Devolve a descrição geral do fae.
     *
     * @return caraterísticas do fae
     */
    @Override
    public String toString() {

        return String.format("###FAE###%n%s", utilizador.toString());
    }

    /**
     * Compara o fae com o objeto recebido.
     *
     * @param obj o objeto a comparar com o fae
     * @return true se o objeto recebido representar um fae equivalente ao fae. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        FAE f = (FAE) obj;

        return this.utilizador.equals(f.utilizador) && this.candidaturasAvaliadas == f.candidaturasAvaliadas;

    }

    /**
     * Devolve um inteiro para comparar dois FAE's pelo seu número de candidaturas avaliadas
     * @param o o fae a ser comparado 
     * @return 1 se a experiência do FAE que é passado por parâmetro é menor que a experiência do FAE atual
     * 0 se as experiências forem iguais
     * -1 se a experiência do FAE atual é menor que a do FAE passado por parâmetro
     */
    @Override
    public int compareTo(FAE o) {
        if (this.candidaturasAvaliadas > o.candidaturasAvaliadas) {
            return 1;
        } else if (this.candidaturasAvaliadas < o.candidaturasAvaliadas) {
            return -1;
        }
        
        return 0;
    }

}
