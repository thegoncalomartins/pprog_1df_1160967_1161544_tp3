/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Algoritmo de Atribuição por Experiência Profissional dos FAE's
 * @author m
 */
public class AlgoritmoAtribuicaoExperienciaProfissional implements AlgoritmoAtribuicao, Serializable {
    
    /**
     * Construtor.
     */
    public AlgoritmoAtribuicaoExperienciaProfissional() {
        
    }
    
    /**
     * Algoritmo de atribuição com base na experiência profissional dos FAE's. 
     * Quanto mais experiência profissonal um FAE tiver, mais candidaturas terá de avaliar.
     * @param evento o evento que possuem as candidaturas a serem distribuídas pelos FAE's
     * @return a lista de atribuições geradas
     */
    @Override
    public ArrayList<Atribuicao> atribui(Evento evento) {
        
        ArrayList<Atribuicao> atribuicoes = new ArrayList();
        ArrayList<Candidatura> lc = evento.getListaCandidaturas().getCandidaturas();
        ArrayList<FAE> lf = evento.getListaFAE().getFae();
        
        // o total de candidaturas ao evento (ainda não avaliadas)
        int totalCandidaturasEvento = evento.getListaCandidaturas().getCandidaturas().size();
        int f = lf.size();
        
        // o total de candidaturas já avaliadas por todos os FAE's
        int totalExperiencia = calcularTotalCandidaturasAvaliadas(lf);
        // contador de candidaturas atribuidas até ao momento
        int candidaturasAtribuidas = 0;
        // numero de candidaturas a atribuir a cada FAE
        int numeroCandidaturasAAtribuir;
        
        // ordena a lista de fae's pelo número de candidaturas avaliadas, ou seja, os FAE's com maior experiência aparecem primeiro
        Collections.sort(lf);
        
        // cria uma cópia da lista de candidaturas feitas ao evento e baralha-a, isto para garantir que as atribuições são sempre feitas de modo aleatório
        // e depois o utilizador torna como definitiva a que desejar
        ArrayList<Candidatura> lc2 = new ArrayList<>(lc);
        Collections.shuffle(lc2);
        
        // distribui as candidaturas pelos FAE's, exceto o último da lista
        for (int i = 0; i < f - 1; i++) {
            numeroCandidaturasAAtribuir = calcularCandAvaliar(lf.get(i), totalCandidaturasEvento, totalExperiencia);
            for (int j = candidaturasAtribuidas; j < candidaturasAtribuidas + numeroCandidaturasAAtribuir; j++) {
                atribuicoes.add(new Atribuicao(lc2.get(j), lf.get(i)));
            }
            candidaturasAtribuidas = candidaturasAtribuidas + numeroCandidaturasAAtribuir;
        }
        
        // distribuir as candidaturas restantes pelo último FAE da lista...
        for (int i = candidaturasAtribuidas; i < totalCandidaturasEvento; i++) {
            atribuicoes.add(new Atribuicao(lc2.get(i), lf.get(f-1)));
            candidaturasAtribuidas++;
        }
        
        // usei isto para testar o algoritmo e se todas as candidaturas foram atribuidas...
        if (candidaturasAtribuidas != totalCandidaturasEvento) {
            System.out.println("Ocorreu um problema...");
        }
        
        return atribuicoes;
    }
    
    /**
     * 
     * @return string com informação sobre o algoritmo, neste caso o seu nome
     */
    @Override
    public String toString() {
        return String.format("%s", "Atribuição por Experiência Profissional");
    }

    /**
     * Calcula o número de candidaturas que um FAE terá de avaliar, com base na sua experiência, em proporção ao número total de candidaturas
     * @param f o FAE
     * @param totalCand o total de candidaturas feitas ao evento
     * @param totalExperiencia o total de candidaturas avaliadas por todos os FAE's (experiência total)
     * @return o número de candidaturas que um FAE terá de avaliar, com base na sua experiência
     */
    private int calcularCandAvaliar(FAE f, int totalCand, int totalExperiencia) {
        return (f.getCandidaturasAvaliadas() * totalCand) / totalExperiencia;
    }

    /**
     * Calcula a experiência total dos FAE's agregados a um evento.
     * @param lf a lista que contém os FAE's
     * @return o número total de candidaturas já avaliadas por todos os FAE's
     */
    private int calcularTotalCandidaturasAvaliadas(ArrayList<FAE> lf) {
        int cont = 0;
        
        for (FAE f : lf) {
            cont += f.getCandidaturasAvaliadas();
        }
        
        return cont;
    }
}
