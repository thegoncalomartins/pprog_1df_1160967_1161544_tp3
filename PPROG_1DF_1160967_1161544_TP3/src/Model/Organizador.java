/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 * A classe Organizador.
 * @author m
 */
public class Organizador implements Serializable{
    
    /**
     * O utilizador
     */
    private Utilizador utilizador;
    
    /**
     * Construtor que recebe o utilizador como parâmetro.
     * 
     * @param utilizador o utilizador 
     */
    public Organizador(Utilizador utilizador) {
        
        this.utilizador = utilizador;
    }
    
    /**
     * Devolve o utilizador.
     * 
     * @return o utilizador 
     */
    public Utilizador getUtilizador() {
        
        return utilizador;
    }
    
    /**
     * Devolve a descrição geral do organizador.
     *
     * @return caraterísticas do organizador
     */
    @Override
    public String toString() {
        
        return String.format("ORGANIZADOR:%n%s", utilizador.toString());
    }
    
    /**
     * Compara o organizador com o objeto recebido.
     *
     * @param obj o objeto a comparar com o organizador
     * @return true se o objeto recebido representar um organizador equivalente ao
     *         organizador. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object obj) {
        
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        
        Organizador o = (Organizador) obj;
        
        return this.utilizador.equals(o.utilizador);
    }
    
    
}
