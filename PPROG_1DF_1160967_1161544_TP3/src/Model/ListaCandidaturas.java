/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A classe de domínio que contém a lista de candidaturas de um evento.
 * 
 * @author m
 */
public class ListaCandidaturas implements Serializable{

    /**
     * A lista de candidaturas.
     */
    private final ArrayList<Candidatura> candidaturas;

    /**
     * Construtor que instância a lista de candidaturas.
     */
    public ListaCandidaturas() {

        candidaturas = new ArrayList<>();
    }

    /**
     * Devolve a lista de candidaturas.
     *
     * @return a lista de candidaturas
     */
    public ArrayList<Candidatura> getCandidaturas() {

        return candidaturas;
    }

    /**
     * Devolve uma nova candidatura com os dados recebidos por parâmetro.
     *
     * @param empresa a empresa
     * @param morada a morada
     * @param telemovel o nº de telemóvel
     * @param areaExposicao a área de exposição
     * @param numeroConvidados o nº de convidados
     * @return a nova candidatura
     */
    public Candidatura novaCandidatura(String empresa, String morada, String telemovel, float areaExposicao, int numeroConvidados) {

        Candidatura c = new Candidatura(empresa, morada, telemovel, areaExposicao, numeroConvidados);
        return c;
    }

    /**
     * Método que procura uma candidatura pelo nome da empresa.
     * @param empresa a empresa a ser procurada
     * @return o objeto candidatura ou null se não foi encontrado
     */
    public Candidatura procurarCandidaturaPorEmpresa(String empresa) {
        for (Candidatura c : candidaturas) {
            if (c.getEmpresa().equalsIgnoreCase(empresa)) {
                return c;
            }
        }

        return null;
    }
    
    /**
     * 
     * @return o tamanho da lista de candidaturas 
     */
    public int tamanho() {
        return candidaturas.size();
    }

    /**
     * Devolve a candidatura situada na posição passada por parâmetro.
     * @param indice índice da candidatura na lista
     * @return o objeto candidatura correspondente ao índice passado por parâmetro
     */
    public Candidatura obterCandidatura(int indice) {
        return candidaturas.get(indice);
    }
    
    /**
     * Adiciona uma candidatura recebida por parâmetro à lista de candidaturas.
     *
     * @param c a candidtura a ser adicionada
     * @return true se a candidatura não existir na lista e for adicionada, false caso contrário
     */
    public boolean registarCandidatura(Candidatura c) {

        if (!candidaturas.contains(c)) {
            return candidaturas.add(c);
        }
        return false;
    }
}
