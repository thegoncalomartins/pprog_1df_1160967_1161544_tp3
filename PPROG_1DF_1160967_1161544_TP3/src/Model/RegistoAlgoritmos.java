/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A classe responsável por conter o registo de Algoritmos de atribuição do centro de eventos.
 * 
 * @author m
 */
public class RegistoAlgoritmos implements Serializable {
    
    /**
     * A lista de algoritmos.
     */
    private ArrayList<AlgoritmoAtribuicao> algoritmos;
    
    /**
     * Construtor que instancia a lista de algoritmos.
     */
    public RegistoAlgoritmos() {
        
        algoritmos = new ArrayList<>();
    }
    
    /**
     * Devolve o algoritmo de atribuição correspondente ao índice.
     * @param indice a posição do algoritmo na lista
     * @return o algoritmo de atribuição correspondente à posição (índice)
     */
    public AlgoritmoAtribuicao obterAlgoritmo(int indice) {
        return algoritmos.get(indice);
    }
    
    /**
     * Devolve o tamanho da lista de algoritmos.
     * 
     * @return o tamanho da lista de algoritmos
     */
    public int tamanho() {
        return algoritmos.size();
    }
    
    /**
     * Adiciona um algoritmo recebido por parâmetro à lista de algoritmos.
     * 
     * @param algoritmo o algoritmo a ser adicionado à lista
     * @return true se o algoritmo não existir na lista e for adicionado, false caso contrário
     */
    public boolean adicionarAlgoritmo(AlgoritmoAtribuicao algoritmo) {
        
        if (!algoritmos.contains(algoritmo)) {
            return algoritmos.add(algoritmo);
        }
        return false;
    }
    
}
