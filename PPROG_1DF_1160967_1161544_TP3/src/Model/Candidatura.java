/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A classe candidatura.
 * @author m
 */
public class Candidatura implements Serializable {

    /**
     * A empresa.
     */
    private String empresa;

    /**
     * A morada.
     */
    private String morada;

    /**
     * O nº de telemóvel.
     */
    private String telemovel;

    /**
     * A área de exposição.
     */
    private float areaExposicao;

    /**
     * A lista de produtos a expor.
     */
    private List<Produto> produtosAExpor;

    /**
     * O nº de convidados.
     */
    private int numeroConvidados;

    /**
     * A lista de decisões da candidatura.
     */
    private ArrayList<Decisao> decisoes;

    /**
     * A empresa por omissão.
     */
    private static final String EMPRESA_POR_OMISSAO = "Sem empresa";

    /**
     * A morada por omissão.
     */
    private static final String MORADA_POR_OMISSAO = "Sem morada";

    /**
     * O nº de telemóvel por omissão.
     */
    private static final String TELEMOVEL_POR_OMISSAO = "000000000";

    /**
     * A área de exposição por omissão.
     */
    private static final float AREA_EXPOSICAO_POR_OMISSAO = 0f;

    /**
     * Os produtos a expor por omissão.
     */
    private static List<Produto> PRODUTOS_A_EXPOR_POR_OMISSAO = new ArrayList<>();

    /**
     * O nº de convidados por omissão.
     */
    private static final int NUMERO_CONVIDADOS_POR_OMISSAO = 0;

    /**
     * O número de dígitos de um nº de telemóvel português válido.
     */
    private static final int NUMERO_DIGITOS_TELEMOVEL_PORTUGAL = 9;

    /**
     * Construtor completo.
     *
     * @param empresa a empresa
     * @param morada a morada
     * @param telemovel o nº de telemóvel
     * @param areaExposicao a área de exposição
     * @param numeroConvidados o nº de convidados
     */
    public Candidatura(String empresa, String morada, String telemovel, float areaExposicao, int numeroConvidados) {

        this.empresa = empresa;
        this.morada = morada;
        setTelemovel(telemovel);
        this.areaExposicao = areaExposicao;
        this.produtosAExpor = new ArrayList<>();
        this.numeroConvidados = numeroConvidados;
        this.decisoes = new ArrayList<>();

    }

    /**
     * Construtor vazio.
     */
    public Candidatura() {

        empresa = EMPRESA_POR_OMISSAO;
        morada = MORADA_POR_OMISSAO;
        telemovel = TELEMOVEL_POR_OMISSAO;
        areaExposicao = AREA_EXPOSICAO_POR_OMISSAO;
        produtosAExpor = PRODUTOS_A_EXPOR_POR_OMISSAO;
        numeroConvidados = NUMERO_CONVIDADOS_POR_OMISSAO;
        decisoes = new ArrayList<>();
    }

    /**
     * Devolve a empresa.
     *
     * @return a empresa
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Devolve a morada.
     *
     * @return a morada
     */
    public String getMorada() {
        return morada;
    }

    /**
     * Devolve o nº de telemóvel.
     *
     * @return o nº de telemovel
     */
    public String getTelemovel() {
        return telemovel;
    }

    /**
     * Devolve a área de exposição.
     *
     * @return a área de exposição
     */
    public float getAreaExposicao() {
        return areaExposicao;
    }

    /**
     * Devolve os produtos a expor.
     *
     * @return os produtos a expor
     */
    public List<Produto> getProdutosAExpor() {
        return produtosAExpor;
    }

    /**
     * Devolve o nº de convidados.
     *
     * @return o nº de convidados
     */
    public int getNumeroConvidados() {
        return numeroConvidados;
    }

    /**
     * Devolve a lista de decisões da candidatura.
     *
     * @return a lista de decisões da candidatura
     */
    public ArrayList<Decisao> getDecisoes() {
        return new ArrayList<>(decisoes);
    }

    /**
     * Altera a empresa.
     *
     * @param empresa a nova empresa
     */
    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    /**
     * Altera a morada.
     *
     * @param morada a nova morada
     */
    public void setMorada(String morada) {
        this.morada = morada;
    }

    /**
     * Altera o nº de telemóvel.
     *
     * @param telemovel o novo nº de telemóvel
     */
    public void setTelemovel(String telemovel) {
        if (telemovel.length() < NUMERO_DIGITOS_TELEMOVEL_PORTUGAL) {
            throw new IllegalArgumentException("O número de telemóvel introduzido é inválido!");
        }
        this.telemovel = telemovel;
    }

    /**
     * Altera a área de exposição.
     *
     * @param areaExposicao a nova área de exposição
     */
    public void setAreaExposicao(float areaExposicao) {
        this.areaExposicao = areaExposicao;
    }

    /**
     * Altera os produtos a expor.
     *
     * @param produtosAExpor os novos produtos a expor
     */
    public void setProdutosAExpor(List<Produto> produtosAExpor) {
        this.produtosAExpor = produtosAExpor;
    }

    /**
     * Altera o nº de convidados.
     *
     * @param numeroConvidados o novo nº de convidados
     */
    public void setNumeroConvidados(int numeroConvidados) {
        this.numeroConvidados = numeroConvidados;
    }

    /**
     * Adiciona um produto à lista de produtos da candidatura.
     *
     * @param p o produto a adicionar à lista de produtos a expor da Candidatura
     */
    public void adicionarProdutoAExpor(Produto p) {

        if (produtosAExpor.contains(p)) {
            throw new ProdutoJaExistenteException(p);
        }
        produtosAExpor.add(new Produto(p));
    }

    /**
     * Adiciona uma nova decisão à lista de decisões da candidatura.
     *
     * @param dec a nova decisão a ser adicionada
     */
    public void adicionarDecisao(Decisao dec) {
        decisoes.add(new Decisao(dec));
    }

    /**
     * Devolve a descrição geral da candidatura.
     *
     * @return caraterísticas da candidatura
     */
    @Override
    public String toString() {

        return String.format("EMPRESA: %s"
                + "%nMORADA: %s"
                + "%nTELEMÓVEL: %s"
                + "%nÁREA EXPOSIÇÃO: %.2f m2"
                + "%nPRODUTOS A EXPOR: %s"
                + "%nNÚMERO DE CONVIDADOS: %d%n", empresa, morada, telemovel, areaExposicao, produtosAExpor, numeroConvidados);
    }

    /**
     * Compara a candidatura com o objeto recebido.
     *
     * @param obj o objeto a comparar com a candidatura
     * @return true se o objeto recebido representar uma candidatura equivalente à candidatura. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Candidatura c = (Candidatura) obj;

        return this.empresa.equalsIgnoreCase(c.empresa) && this.morada.equalsIgnoreCase(c.morada) && this.telemovel.equalsIgnoreCase(c.telemovel)
                && this.areaExposicao == c.areaExposicao && this.produtosAExpor.equals(c.produtosAExpor) && this.numeroConvidados == c.numeroConvidados;
    }

}
