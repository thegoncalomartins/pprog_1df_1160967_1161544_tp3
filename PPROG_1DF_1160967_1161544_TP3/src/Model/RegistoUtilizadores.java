/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A classe responsável por conter o registo de utilizadores do centro de eventos.
 * @author m
 */
public class RegistoUtilizadores implements Serializable {
    
    /**
     * A lista de utilizadores.
     */
    private final ArrayList<Utilizador> utilizadores;
    
    /**
     * Construtor que instância a lista de utilizadores.
     */
    public RegistoUtilizadores() {
        
        utilizadores = new ArrayList<>();
    }
    
    /**
     * Devolve a lista de utilizadores.
     * 
     * @return a lista de utilizadores
     */
    public ArrayList<Utilizador> getUtilizadores() {
        
        return utilizadores;
    }
    
    /**
     * Adiciona um utilizador recebido por parâmetro à lista de utilizadores.
     * 
     * @param utilizador o utilizador a ser adicionado
     * @return true se o utilizador não existir na lista e for adicionado, false caso contrário
     */
    public boolean addUtilizador(Utilizador utilizador) {
        
        if (!utilizadores.contains(utilizador)) {
            return utilizadores.add(utilizador);
        }
        return false;
    }
    
    /**
     * Devolve um utilizador com o username passado por parâmetro. Se não existir nenhum devolve null.
     * 
     * @param username o username do utilizador
     * @return um utilizador com o username passado por parâmetro, null se não existir nenhum
     */
    public Utilizador procurarUtilizadorPorUsername(String username) {
        
        for (Utilizador utilizador: utilizadores) {
            
            if (username.equalsIgnoreCase(utilizador.getUsername())) {
                return utilizador;
            }
        }
        return null;
    }
}
