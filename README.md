# ** Paradigmas da Programação ** #
*(2016/2017)*

## **Enunciado** ##

O trabalho prático de Paradigmas de Programação (PPROG) será realizado em
articulação com a unidade curricular de Engenharia de Software (ESOFT). O
trabalho de PPROG consiste na implementação das funcionalidades definidas nos
seguintes Casos de Uso do projeto que está a ser desenvolvido em ESOFT,
intitulado “Centro de Eventos”:

* Caso de Uso UC3 – Atribuir Candidatura
* Caso de Uso UC4 – Decidir Candidatura
* Caso de Uso UC5 – Submeter Candidatura

Na implementação devem ser considerados os requisitos da iteração 2 do
trabalho de ESOFT.
O enunciado, assim como os requisitos, podem ser consultados no site
https://bitbucket.org/nunopsilva/projeto-esoft-pprog-2016-2017/wiki/Home.
Este repositório é partilhado pelo que o seu conteúdo não deve ser alterado.
Para que seja possível testar as funcionalidades implementadas será necessário
desenvolver uma classe de teste, responsável pela criação de instâncias das
diferentes classes definidas no modelo de design, como por exemplo, Gestor de
Eventos, Utilizador, FAE, Participante, Organizador, entre outras.

** Os requisitos funcionais da implementação incluem: **
* implementação das funcionalidades descritas nos UC3, UC4 e UC5;
* implementação de três mecanismos alternativos de atribuição de
candidaturas baseados em diferentes critérios (e.g. número de FAE
pretendidos por candidatura, distribuição de carga equitativa pelos FAE,
experiência profissional na realização de exposições) - o resultado da
atribuição pode ser alterado através da escolha de diferentes mecanismos
até que um seja confirmado como definitivo pelo organizador;
* implementação de um mecanismo que permita garantir a persistência dos
dados, através do seu armazenamento num ficheiro, e posterior
recuperação (implementação de mecanismos de leitura e de escrita);
* implementação de uma interface com o utilizador que permita usar as funcionalidades implementadas;
* a interface poderá ser baseada em consola ou poderá consistir numa interface gráfica (esta última com valorização adicional);
* implementação de uma classe de teste;
* implementação de um mecanismo de instanciação de classes a partir do conteúdo de um ficheiro de texto
* o ficheiro de texto deverá estar disponível no momento da apresentação contendo informação para a instanciação de todas as classes declaradas.

** Os requisitos não funcionais incluem: **
* desenvolvimento em linguagem Java e IDE Netbeans;
* documentação do código, usando a ferramenta Javadoc;
* diagrama de classes em formato PDF;
* documento contendo a descrição das funcionalidades implementadas;
* utilização do repositório Bitbucket e da ferramenta de controlo de versões Git
* o repositório deverá ser privado e partilhado com o professor das aulas práticas
* o nome do repositório deve respeita o seguinte formato: PPROG_Turma_NúmeroAluno1_NúmeroAluno2_TP3.